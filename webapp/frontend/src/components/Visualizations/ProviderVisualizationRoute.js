import React, { useEffect } from 'react';
import styled from 'styled-components';
import AthletePerCountry from "./AthletePerCountry";

import RegionMedalBarChart from "./RegionMedalBarChart";
import { MedalPieChart } from "./MedalPieChart";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

const TitleWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 1rem;
  margin: 1rem 0;
  background-color: lightgray;
  font-weight: bold;
`;

const VisualWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const ProviderVisualizationRoute = () => {
  return (
    <MainWrapper>
      <TitleWrapper>
        <h1>Going for Gold Visualizations</h1>
      </TitleWrapper>
      <VisualWrapper>
        <h4> Region vs. Num Medals </h4>
        <RegionMedalBarChart />
      </VisualWrapper>
      <VisualWrapper>
        <h4> Medals per Country (top 20) </h4>
        <MedalPieChart />
      </VisualWrapper>
      <VisualWrapper>
        <h4> Number of Athletes per Country (top 15) </h4>
        <AthletePerCountry />
      </VisualWrapper>
    </MainWrapper>
  )
}

export default ProviderVisualizationRoute
