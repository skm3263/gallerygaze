import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import ArtworkCard from '../reuseable/ArtworkCard';
import { Button } from '@mui/material';
import SearchBar from '../reuseable/SearchBar';
import FilterSelect from '../reuseable/FilterSelect';
import { DataGrid } from '@mui/x-data-grid';

const MainWrapper = styled.div``;

const CardsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const BottomRow = styled.div`
  display: flex;
  justify-content: end;
  margin: 1rem 8rem;
  p {
    margin-right: 1rem;
  }
`;

const SearchWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const columns = [
  { field: 'name', headerName: 'Name', flex: 1 },
  { field: 'media', headerName: 'Media', flex: 1 },
  { field: 'movement_name', headerName: 'Movement', flex: 1 },
  { field: 'date_created', headerName: 'Creation Date', flex: 1 },
  { field: 'popularity_ranking', headerName: 'Popularity Ranking', flex: 1 },
];

const ArtworkTable = (props) => {
  const [data, setData] = useState({ entries: [], total: 0 });
  const [pageNum, setPageNum] = useState(0);
  const [searchVal, setSearchVal] = useState('');
  const [instancesPerPage, setInstancesPerPage] = useState(25);
  const [selectedFilterState, setSelectedFilterState] = useState({});
  const [filterParamString, setFilterParamString] = useState('');
  const [totalInstances, setTotalInstances] = useState(0);
  const [sortParamString, setSortParamString] = useState('');

  const artworkFilterTitles = [
    'Name',
    'Era',
    'Movement',
    'Media',
    'Popularity Ranking',
  ];

  const artworkFilters = [
    ['A-E', 'F-L', 'M-P', 'Q-Z'],
    ['1300s', '1400s', '1500s', '1600s', '1700s', '1800s', '1900s'],
    [
      'Romanticism',
      'Northern Renaissance',
      'Baroque',
      'Realism',
      'Expressionism',
      'Impressionism',
      'Post-Impressionism',
      'High Renaissance',
      'Mannerism (Late Renaissance)',
      'Other',
    ],
    [
      'Oil',
      'Canvas',
      'Panel',
      'Unknown',
      'Tempera',
      'Fresco',
      'Wood',
      'Paper',
      'Watercolor',
      'Other',
    ],
    ['1-100', '101-200', '201-300', '301-400', '401-500', '501+'],
  ];

  useEffect(() => {
    updateTableData(pageNum, instancesPerPage);
  }, [
    pageNum,
    instancesPerPage,
    searchVal,
    selectedFilterState,
    filterParamString,
    sortParamString,
  ]);

  const filterMapping = {
    era: 'date_created',
    movement: 'movement_name',
  };

  const mapFilterValues = (filter) => {
    if (filter in filterMapping) {
      return filterMapping[filter];
    }
    return filter;
  };

  const handleSearch = (searchValue) => {
    console.log('searching for ' + searchValue);
    setSearchVal(searchValue);
    setPageNum(0);
  };

  const onCheckboxClick = (title, value) => {
    let newState = selectedFilterState;

    if (title in newState) {
      if (newState[title].includes(value)) {
        newState[title] = newState[title].filter((item) => item !== value);
        if (newState[title].length === 0) {
          delete newState[title];
        }
      } else {
        newState[title] = [...newState[title], value];
      }
    } else {
      newState[title] = [value];
    }
    let filterParams = `&filter=`;
    for (let key in newState) {
      const newKey = String(key).replaceAll(' ', '_');
      filterParams += `${mapFilterValues(
        String(newKey).toLowerCase()
      )}=${newState[key]
        .map((e) => mapFilterValues(e))
        .join(',')
        .toLowerCase()};`;
    }
    // get rid of last ;
    filterParams = filterParams.slice(0, -1);

    if (filterParams !== '&filter=') {
      setFilterParamString(filterParams.replace("+", "%2B"));
    } else {
      setFilterParamString('');
    }

    setSelectedFilterState(newState);
  };

  const updateTableData = (page, pageSize) => {
    console.log('Updating table');
    const newPageNum = page + 1;
    let query = `https://api.beta.gallerygaze.me/search?model=artwork${
      searchVal !== '' ? `&general_string=${searchVal}` : ''
    }&page=${newPageNum}&maxResults=${pageSize}`;

    query += filterParamString;
    query += sortParamString;
    console.log(String(query));

    // remove &filter if there are none selected
    const endVal = query.length;
    if (query.slice(endVal - 7, endVal) === '&filter') {
      query = query.slice(0, endVal - 7);
    }

    console.log(query);
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((res) => {
        console.log(res);
        setTotalInstances(res.total);
        res.entries = res['query results'];
        setData(res);
      })
      .catch((err) => console.error('ERROR FETCH: ', err));
  };

  const sortByColumn = (e) => {
    if (e.length <= 0) return;

    let sortParam = `&sort=${e[0].field}&sortReverse=${
      e[0].sort === 'asc' ? 'false' : 'true'
    }`;
    setSortParamString(sortParam);
  };

  return (
    <MainWrapper>
      <FilterSelect
        filterTitles={artworkFilterTitles}
        modelFilters={artworkFilters}
        onClick={onCheckboxClick}
      />
      <SearchWrapper>
        <SearchBar onSearch={handleSearch} />
      </SearchWrapper>
      <DataGrid
        rows={[]}
        columns={columns}
        onSortModelChange={sortByColumn}
        hideFooter={true}
        rowCount={0}
        components={{ NoRowsOverlay: 'asdf' }}
      />
      <br />
      <br />
      <br />
      <CardsWrapper>
        {data.entries.map((artwork) => (
          <ArtworkCard key={artwork.id} data={artwork} searchTerm={searchVal} />
        ))}
      </CardsWrapper>
      <BottomRow>
        <p>Rows per page: {instancesPerPage}</p>
        <p>
          {pageNum * instancesPerPage}-
          {Math.min(
            totalInstances,
            pageNum * instancesPerPage + instancesPerPage
          )}{' '}
          of {totalInstances}{' '}
        </p>
        <Button
          disabled={pageNum === 0}
          onClick={() => setPageNum(pageNum - 1)}
        >
          Prev
        </Button>
        <Button
          disabled={pageNum === Math.floor(totalInstances / instancesPerPage)}
          onClick={() => setPageNum(pageNum + 1)}
        >
          Next
        </Button>
      </BottomRow>
    </MainWrapper>
  );
};

export default ArtworkTable;
