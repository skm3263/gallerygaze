import React, { useEffect, useState } from 'react';
import { PieChart, Pie, ResponsiveContainer } from 'recharts';

export const MedalPieChart = () => {
  const [medalTotalData, setMedalTotalData] = useState([]);
  const [medalData, setMedalData] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const query = `https://api.goingforgold.me/api/countries?p=-1`;
      let response = await fetch(query);
      let json = await response.json();
      let dictionary = {};

      for (let entry in json['body']) {
        const contry = json['body'][entry]['country_name'];
        const num_gold = json['body'][entry]['country_num_gold'];
        const num_silver = json['body'][entry]['country_num_silver'];
        const num_bronze = json['body'][entry]['country_num_bronze'];
        const total_medals = json['body'][entry]['country_medals_total'];

        dictionary[contry] = {
          gold: num_gold,
          silver: num_silver,
          bronze: num_bronze,
          total: total_medals,
        };
      }

      // keep top 20 countries by total excluding Totals
      let top20 = Object.keys(dictionary)
        .sort((a, b) => {
          if (a === 'Totals') {
            return 1;
          }
          return dictionary[b].total - dictionary[a].total;
        })
        .slice(0, 20);

      const totalMedalData = [];
      const medalsData = [];
      top20.forEach((country) => {
        totalMedalData.push({
          name: country,
          total: dictionary[country].total,
          // fill random color
          fill: '#' + Math.floor(Math.random() * 16777215).toString(16),
        });
        medalsData.push({
          name: country + ' - Gold',
          value: dictionary[country].gold,
          fill: '#d4af37', // hex gold color #d4af37
        });
        medalsData.push({
          name: country + ' - Silver',
          value: dictionary[country].silver,
          fill: '#c0c0c0', // hex silver color #c0c0c0
        });
        medalsData.push({
          name: country + ' - Bronze',
          value: dictionary[country].bronze,
          fill: '#cd7f32', // hex bronze color #cd7f32
        });
      });
      setMedalTotalData(totalMedalData);
      setMedalData([{ data: medalsData }]);
    }

    fetchData();
  }, []);

  const data01 = [
    { name: 'Group A', value: 400 },
    { name: 'Group B', value: 300 },
    { name: 'Group C', value: 300 },
    { name: 'Group D', value: 200 },
  ];
  const data02 = [
    { name: 'A1', value: 100 },
    { name: 'A2', value: 300 },
    { name: 'B1', value: 100 },
    { name: 'B2', value: 80 },
    { name: 'B3', value: 40 },
    { name: 'B4', value: 30 },
    { name: 'B5', value: 50 },
    { name: 'C1', value: 100 },
    { name: 'C2', value: 200 },
    { name: 'D1', value: 150 },
    { name: 'D2', value: 50 },
  ];

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = (props) => {
    const { cx, cy, midAngle, innerRadius, outerRadius, percent, index } =
      props;

    const radius = innerRadius + (outerRadius - innerRadius)
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <>
        <text
          x={x}
          y={y}
          fill='black'
          textAnchor={x > cx ? 'start' : 'end'}
          dominantBaseline='central'
        >
          {props.name}
        </text>
      </>
    );
  };

  return (
    <ResponsiveContainer width='100%' height={550}>
      <PieChart>
        
        {medalData.map((s) => (
          <Pie
            data={s.data}
            dataKey='value'
            cx='50%'
            cy='50%'
            innerRadius={'75%'}
            outerRadius={'100%'}
            fill='#000'
            labelLine={false}
            key={1}
          />
        ))}

        <Pie
          data={medalTotalData}
          dataKey='total'
          cx='50%'
          cy='50%'
          outerRadius={'75%'}
          fill='green'
          labelLine={false}
          label={renderCustomizedLabel}
        />
        
      </PieChart>
    </ResponsiveContainer>
  );
};
