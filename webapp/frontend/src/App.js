import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Footer from './components/Footer';
import Home from './components/HomeRoute';
import Navbar from './components/Navbar.js';
import styled from 'styled-components';
import AboutRoute from "./components/AboutRoute";
import ArtistFinderRoute from "./components/Artists/ArtistFinderRoute";
import ArtworkFinderRoute from "./components/Artworks/ArtworkFinderRoute";
import MuseumFinderRoute from "./components/Museums/MuseumFinderRoute";
import ArtworkDataRoute from "./components/Artworks/ArtworkDataRoute";
import ArtistDataRoute from "./components/Artists/ArtistDataRoute";
import MuseumDataRoute from "./components/Museums/MuseumDataRoute";
import GlobalFinderRoute from "./components/GlobalSearch/GlobalFinderRoute";
import VisualizationRoute from "./components/Visualizations/VisualizationRoute";
import ProviderVisualizationRoute from "./components/Visualizations/ProviderVisualizationRoute";
import MovementFinderRoute from "./components/Movements.js/MovementFinderRoute";
import MovementDataRoute from "./components/Movements.js/MovementDataRoute";

const GlobalWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

const MainWrapper = styled.div`
  flex: 1;
`;

function App() {
  return (
    <Router>
      <GlobalWrapper>
        <Navbar />
        <MainWrapper>
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/search' exact component={GlobalFinderRoute} />
            <Route path='/artworks' exact component={ArtworkFinderRoute} />
            <Route path='/artists' exact component={ArtistFinderRoute} />
            <Route path='/museums' exact component={MuseumFinderRoute} />
            <Route path='/movements' exact component={MovementFinderRoute} />
            <Route path='/about' exact component={AboutRoute} />
            <Route path='/artworks/:id' exact component={ArtworkDataRoute} />
            <Route path='/artists/:id' exact component={ArtistDataRoute} /> 
            <Route path='/museums/:id' exact component={MuseumDataRoute} />
            <Route path='/movements/:id' exact component={MovementDataRoute} />
            <Route path='/visualizations' exact component={VisualizationRoute} />
            <Route path='/provider-visualizations' exact component={ProviderVisualizationRoute} />
          </Switch>
        </MainWrapper>
        <Footer />
      </GlobalWrapper>
    </Router>
  );
}

export default App;
