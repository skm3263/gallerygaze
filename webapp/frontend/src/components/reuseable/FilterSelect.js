import React, {useState, useEffect} from 'react'
import styled from 'styled-components';

/* 
  CITATION: Code to filter on each model was helped out by
  viewing the Spring '21 10am group Where Art Thou's repo.
  Though essentially all code is completely our own, viewing
  their code did help get the process going. Thanks to them and
  their work. View each model table page for more look into our
  implementation.
  Link: https://gitlab.com/cs373-group-5/where-art-thou/
*/

const FilterTitle = styled.div`
  font-size: 20px;
  font-weight: bold;  
`  

const FilterSelect = (props) => {
  const {onClick} = props;
  
  return (
    <form
    style = {{
      width: "100%",
      display: "flex",
      justifyContent: "space-around",
      textAlign: "left",
    }}>

      {/* Filter Options */}
        {props.filterTitles.map((title, titleIdx) => (
          <div key={titleIdx}>
            <FilterTitle>{title}</FilterTitle>
            {props.modelFilters[titleIdx].map((filterOption, optionIdx) => (
              <label key={optionIdx}>
                <input
                  type = "checkbox"
                  label = {filterOption}
                  onChange = {() => onClick(title, filterOption)}
                />
                {filterOption}
                <br/>
              </label>
            ))}
          </div>
        ))}
    </form>
  )

}

export default FilterSelect
