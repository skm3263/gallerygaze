import React, { useEffect, useState } from 'react';
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';
import Spinner from "../reuseable/Spinner";

const RegionMedalBarChart = () => {
  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      let regionData = {};

      const query = `https://api.goingforgold.me/api/countries?p=-1`;
      let response = await fetch(query);
      let json = await response.json();
      for (let entry in json['body']) {
        let region = json['body'][entry].country_region;
        if (regionData[region]) {
          regionData[region] += json['body'][entry].country_medals_total;
        } else {
          regionData[region] = json['body'][entry].country_medals_total;
        }
      }
      let newData = [];
      for (let key in regionData) {
        if (key === 'null') continue;
        newData.push({
          name: key,
          medals: regionData[key],
        });
      }
      // sort newData by ranking
      newData.sort((a, b) => {
        return b.medals - a.medals;
      });
      // keep only top 25
      newData = newData.slice(0, 20);
      setData(newData);
      setLoading(false);
    }
    fetchData();
  }, []);

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const tickFormatter = (value, index) => {
    const limit = 8; // put your maximum character
    if (value.length < limit) return value;
    return `${value.toString().substring(0, limit)}...`;
  };

  return (
    <>
    {loading && <Spinner />}
    {!loading && (
    <ResponsiveContainer width='100%' height={300}>
      <BarChart
        width={2000}
        height={300}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray='3 3' />
        <XAxis dataKey='name' tickFormatter={tickFormatter} />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey='medals' fill='#FF7F7F' />
      </BarChart>
    </ResponsiveContainer> )}
    </>
  );
};

export default RegionMedalBarChart;
