from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import backref

db = SQLAlchemy()

artist_to_museum = db.Table(
    "artist_to_museum",
    db.Column("artist_id", db.String(), db.ForeignKey("artist.id")),
    db.Column("museum_id", db.String(), db.ForeignKey("museum.id")),
)


class Artist(db.Model):
    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String())
    image = db.Column(db.String())
    bio = db.Column(db.Text())
    birthday = db.Column(db.String())
    deathday = db.Column(db.String())
    gender = db.Column(db.String())
    hometown = db.Column(db.String())
    nationality = db.Column(db.String())
    artwork_count = db.Column(db.Integer)
    popularity_ranking = db.Column(db.Integer)
    artworks = db.relationship("Artwork", backref="artist")
    museums = db.relationship("Museum", secondary=artist_to_museum)


class Artwork(db.Model):
    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String())
    image = db.Column(db.String())
    genre = db.Column(db.String())
    movement_name = db.Column(db.String())
    media = db.Column(db.String())
    desc = db.Column(db.Text())
    width = db.Column(db.Integer)
    height = db.Column(db.Integer)
    date_created = db.Column(db.String())
    popularity_ranking = db.Column(db.Integer)
    artist_id = db.Column(db.String(), db.ForeignKey("artist.id"))
    museum_id = db.Column(db.String(), db.ForeignKey("museum.id"))
    movement_id = db.Column(db.String(), db.ForeignKey("movement.id"))


class Museum(db.Model):
    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String())
    image = db.Column(db.String())
    status = db.Column(db.String())
    address = db.Column(db.String())
    city = db.Column(db.String())
    country = db.Column(db.String())
    phone = db.Column(db.String())
    rating = db.Column(db.Float)
    website = db.Column(db.String())
    maps_url = db.Column(db.String())
    hours = db.Column(db.String())
    artwork_count = db.Column(db.Integer)
    popularity_ranking = db.Column(db.Integer)
    lat = db.Column(db.Float)
    lng = db.Column(db.Float)
    artworks = db.relationship("Artwork", backref="museum")
    artists = db.relationship("Artist", secondary=artist_to_museum)


class Movement(db.Model):
    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String())
    image = db.Column(db.String())
    desc = db.Column(db.Text())
    artwork_count = db.Column(db.Integer)
    artworks = db.relationship("Artwork", backref="movement")
