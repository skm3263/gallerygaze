# gallerygaze
Group Members (GitLab ID, EID)
---
Shan Memon: skm3263, skm3263  
Soham Roy: Soham3-1415, sr48397  
Jack Greer: jackgreer79, jgg2669  
Cole Kauppinen: kauppcol, cak3232  
Lance Nguyen: lancenguyen02, lgn297  

Project Information
---
Git SHA: 5d99c04f6c6239ff5f47e1b0bd6e0966c89ac244
Project Leader: Lance Nguyen
Leader Responsibilities: The leader should ensure that all group members contribute sufficiently and all requirements are met.  
GitLab Pipelines: [https://gitlab.com/skm3263/gallerygaze/-/pipelines](https://gitlab.com/skm3263/gallerygaze/-/pipelines)  
Website: [gallerygaze.me](https://www.gallerygaze.me/)

Estimated completion time for each member
---
Shan Memon: 10 hours  
Soham Roy: 10 hours  
Jack Greer: 10 hours  
Cole Kauppinen: 10 hours  
Lance Nguyen: 10 hours

Actual completion time for each member
---
Shan Memon: 15 hours  
Soham Roy: 15 hours  
Jack Greer: 15 hours  
Cole Kauppinen: 15 hours  
Lance Nguyen: 15 hours  

Note: None.
