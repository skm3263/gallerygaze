import React, {useEffect} from 'react';
import styled from 'styled-components';
import ArtistTable from "./ArtistTable";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

function ArtistFinderRoute() {
  useEffect(() => {
    document.title = "Gallery Gaze - Artists"
  }, [])

  return (
    <MainWrapper>
      <h1>Artists</h1>
      <ArtistTable/>
    </MainWrapper>
  );
}

export default ArtistFinderRoute;
