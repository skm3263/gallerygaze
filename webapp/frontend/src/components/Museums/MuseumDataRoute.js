import React, {useState, useEffect} from 'react'
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import museumData from '../../data/museums.json';
import { useParams } from 'react-router-dom';
import Paper from '@mui/material/Paper';
import ArtworkCarousel from "../Artworks/ArtworkCarousel";
import ArtistCarousel from "../Artists/ArtistCarousel";
import Map from "../reuseable/Map"

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

const MiddleSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`

const LeftSide = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 30%;
  `
const RightSide = styled.div`
  width: 50%; 
`

const RedirectLink = styled(Link)`
  text-decoration: none;
  color: black;
`;

const Carousels = styled.div`
  display: flex;
  justify-content: center;
`

const WebsiteWrapper = styled.div`
  display: flex;
  align-items: center;
  a {
    margin-left: 0.25rem;
  }
`
 
const getData = (id) => museumData.find(e => e.museum_id == id)

const MuseumDataRoute = (props) => {
  const { id } = useParams();

  const [data, setData] = useState({});

  const searchVal = props.location.state?.searchTerm ?? '';

  // highlight text that matches search
  const highlightText = (text) => {
    try {
      const textStr = String(text);
      // Split text on highlight term, include term itself into parts, ignore case
      const parts = textStr.split(new RegExp(`(${searchVal})`, 'gi'));
      return <span>{parts.map(part => part.toLowerCase() === searchVal.toLowerCase() ? <mark>{part}</mark> : part)}</span>;
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetch("https://api.beta.gallerygaze.me/museums/" + id)
    .then(response => {
      if (response.ok)
        return response.json();
      throw response
    })
    .then(data => {
      console.log(data)
      setData(data)
    })
    .catch(err => console.error("ERROR FETCH: ", err))
  }, [])

  return (
    <MainWrapper>
      <h1>{highlightText(data.name)}</h1>
      <MiddleSection>
        <LeftSide>
          <img src={data.image} width="500px" alt={data.name}/>
        </LeftSide>
        <RightSide>
          <Paper sx={{padding: '2rem'}}>
            <p> {data.bio && highlightText(data.bio)}</p> 
            <WebsiteWrapper>
              <p> <b>Website:</b></p>
              <a href={data.website}>{highlightText(data.website)}</a>
            </WebsiteWrapper>
            <p> <b>Address:</b> {highlightText(data.address)} </p>
            <p> <b>City:</b> {highlightText(data.city)} </p>
            <p> <b>Country:</b> {highlightText(data.country)} </p>
            <p> <b>Phone Number:</b> {highlightText(data.phone)} </p>
            <p> <b>Status:</b> {highlightText(data.status)}</p>
            <p> <b>Hours:</b> {highlightText(data.hours)}</p>
            <p> <b>Popularity Ranking:</b> {highlightText(data.popularity_ranking)}</p> 
            <p> <b>Rating:</b> {highlightText(data.rating)}</p> 
            <p> <b>Number of Artworks:</b> {highlightText(data.artwork_count)}</p> 
            <Map center={{lat: data.lat, lng: data.lng}}/>
          </Paper>
        </RightSide>
      </MiddleSection>
      <Carousels>
        {data && data.artworks && <ArtworkCarousel data={data.artworks}/>}
        {data && data.artists && <ArtistCarousel data={data.artists}/>}
      </Carousels>
    </MainWrapper>
  );
}

export default MuseumDataRoute;
