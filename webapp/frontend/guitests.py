from selenium import webdriver
import unittest
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

"""
CITATION:

Code for this testing suite was inspired by the WhereArtThou project from
Spring 2021, 10 AM. Thank you to them and their awesome work for helping
us put this group of acceptance tests together.
Link to their repo: https://gitlab.com/cs373-group-5/where-art-thou/-/tree/master
"""

import os
from dotenv import load_dotenv

load_dotenv()
BRANCH_NAME = os.getenv('CI_MERGE_REQUEST_TARGET_BRANCH_NAME') or os.getenv('CI_COMMIT_BRANCH')
MAIN_BRANCH_NAME = os.getenv('MAIN_BRANCH_NAME')
assert BRANCH_NAME is not None
assert MAIN_BRANCH_NAME is not None

PROTO = 'https'
DOMAIN = 'gallerygaze.me'
ENDPOINT = f"{PROTO}://{'' if BRANCH_NAME == MAIN_BRANCH_NAME else 'beta.'}{DOMAIN}"
print(f'ENDPOINT: {ENDPOINT}')

class SeleniumTests(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--window-size=1920,1080')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument("--no-sandbox")
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options = chrome_options)
        self.wait_time = 50
        self.root_url = ENDPOINT
        # self.root_url = "http://localhost:3000"

    def test_header(self): # 1
        driver = self.driver
        driver.get(self.root_url)
        expected = "Gallery Gaze"
        h1 = driver.find_elements_by_tag_name("h1")
        actual = h1[0].text
        self.assertEqual(expected, actual)

    def test_label(self): # 2
        driver = self.driver
        driver.get(self.root_url)
        expected = "Search through your favorite artwork, find your favorite artists, go to your favorite museums!"
        p = driver.find_elements_by_tag_name("p")
        actual = p[0].text
        self.assertEqual(expected, actual)

    def test_artwork_route(self): # 3
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Artwork")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[3]/div[8]/button/div/div')
        expected = "The Glass of Wine"
        actual = link.text
        self.assertEqual(expected, actual)

    def test_artwork_route_grid(self): # 4
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Artwork")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[3]/div[9]/button/div')
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[2]/div[1]/p[3]/span')
        expected = "Baroque"
        actual = link.text
        self.assertEqual(expected, actual)

    def test_artists_route(self): # 5
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/div/div/div[8]/div[1]')
        expected = "Max Beckmann"
        actual = link.text
        self.assertEqual(expected, actual)

    def test_artists_route_table(self): # 6
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/div/div/div[3]/div[1]')
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div[1]/div[2]/div/p[5]')
        expected = "Nationality: Greek"
        actual = link.text
        self.assertEqual(expected, actual)

    def test_museums_route(self): # 7
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div/div/div/div/div[10]/div[1]/span')
        expected = "Museum Ludwig"
        actual = link.text
        self.assertEqual(expected, actual)

    def test_museums_route_table(self): # 8
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/div/div/div[6]/div[1]')
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div[1]/div[2]/div/p[8]')
        expected = "Popularity Ranking: 79"
        actual = link.text
        self.assertEqual(expected, actual)

    def test_about_page(self): # 9
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("About")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div[3]/div[1]/div')
        expected = "API Links & info"
        actual = link.text
        self.assertEqual(expected, actual)        

    def test_about_page_2(self): # 10
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("About")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div[4]/div[2]/div[2]/div[1]').text
        assert "Total team commits: " in link

    def test_filter(self): # 11
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]')
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div/div/div/div/div[2]/div[1]')
        expected = "Albrecht Durer"
        actual = link.text
        self.assertEqual(expected, actual) 

    def test_sorting(self): # 11
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]')
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div/div/div/div/div[2]/div[1]')
        expected = "Albrecht Durer"
        actual = link.text
        self.assertEqual(expected, actual) 

    def test_filtering(self): # 12
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/form/div[1]/label[2]/input')
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div/div/div/div/div[5]/div[1]/span')
        expected = "Hermitage Museum"
        actual = link.text
        self.assertEqual(expected, actual)

    def test_searching(self): # 13
        driver = self.driver
        driver.get(self.root_url)
        link = driver.find_element_by_link_text("Artwork")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[1]/form/div/div/input')
        link.send_keys("Das")
        link.send_keys(Keys.RETURN)
        time.sleep(4)
        link = driver.find_element_by_xpath('/html/body/div/div/div[2]/div/div/div[3]/div[1]/button/div/div/span')
        expected = "Das kranke Kind"
        actual = link.text
        self.assertEqual(expected, actual)

if __name__ == "__main__":
    unittest.main()

