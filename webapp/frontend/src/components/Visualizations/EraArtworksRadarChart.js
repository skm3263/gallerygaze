import React, { useEffect, useState } from 'react';
import { 
  Radar, 
  RadarChart, 
  PolarGrid, 
  PolarAngleAxis, 
  PolarRadiusAxis, 
  ResponsiveContainer } from 'recharts';

const EraArtworksRadarChart = () => {

  useEffect(() => {
    let data = [];
    let eraData = {};

    const query = 'https://api.beta.gallerygaze.me/search?model=artwork&page=1&maxResults=600';
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((res) => {
        data = res['query results'];
        eraData = data.reduce((acc, curr) => {
          let dictVal = curr.date_created.substring(0, 2).concat("00s");
          if (acc[dictVal]) {
            acc[dictVal] += 1;
          } else {
            acc[dictVal] = 1;
          }
          return acc;
        }, {});

        let newData = [];
        for (let key in eraData) {
          newData.push({
            era: key,
            artworks: eraData[key]
          })
        }

        // sort newData by era
        newData.sort((a, b) => {
          if (a.era < b.era) {
            return -1;
          } else if (a.era > b.era) {
            return 1;
          } else {
            return 0;
          }
        });

        setData(newData);
      })
      .catch((err) => console.error('ERROR FETCH: ', err));
  }, []);

  const [data, setData] = useState([]);

  return (
    <ResponsiveContainer width="100%" height={600}>
      <RadarChart
        width={2000}
        height={600}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <PolarGrid/>
        <PolarAngleAxis dataKey = "era" />
        <PolarRadiusAxis/>
        <Radar name="Mike" dataKey = "artworks" stroke="#8884d8" fill="#82ca9d" fillOpacity={0.6} />
      </RadarChart>
    </ResponsiveContainer>
  );
};

export default EraArtworksRadarChart;
