from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
import requests
import json
from dotenv import load_dotenv
import os
from models import *

app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

load_dotenv()
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")
places_key = os.getenv("PLACES_KEY")

db.app = app
db.init_app(app)
# db.create_all()

museumList = []
museumDict = {}
dbMuseums = Museum.query.all()
for dbMuseum in dbMuseums:
    museumDict[dbMuseum.name] = dbMuseum

# detailed_url = 'https://maps.googleapis.com/maps/api/place/details/json?fields=business_status%2Cformatted_address%2Cname%2Cphoto%2Cplace_id%2Curl%2Cinternational_phone_number%2Copening_hours%2Cwebsite%2Crating&place_id=' + 'ChIJD3uTd9hx5kcR1IQvGfr8dbk' + '&key=' + places_key
# data = requests.get(detailed_url).json()
# print(json.dumps(data,indent=2))

dbArtworks = Artwork.query.all()
for dbArtwork in dbArtworks:
    if dbArtwork.museum_name not in museumDict:
        print(dbArtwork.museum_name)
        print(dbArtwork.id)
        place_url = (
            "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="
            + dbArtwork.museum_name
            + "&inputtype=textquery&key="
            + places_key
        )
        place_id = requests.get(place_url).json()["candidates"][0]["place_id"]
        detailed_url = (
            "https://maps.googleapis.com/maps/api/place/details/json?fields=business_status%2Cformatted_address%2Cname%2Cphoto%2Cplace_id%2Curl%2Cinternational_phone_number%2Copening_hours%2Cwebsite%2Crating&place_id="
            + place_id
            + "&key="
            + places_key
        )

        museumData = requests.get(detailed_url).json()
        museumData = museumData["result"]

        photo_url = (
            "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photo_reference="
            + museumData["photos"][0]["photo_reference"]
            + "&key="
            + places_key
            if ("photos" in museumData and len(museumData["photos"]) > 0)
            else None
        )
        phoneNum = (
            museumData["international_phone_number"]
            if "international_phone_number" in museumData
            else None
        )
        hoursList = (
            museumData["opening_hours"]["weekday_text"]
            if "opening_hours" in museumData
            else None
        )
        stat = (
            museumData["business_status"] if "business_status" in museumData else None
        )
        ratingInfo = museumData["rating"] if "rating" in museumData else None
        site = museumData["website"] if "website" in museumData else None
        addr = (
            museumData["formatted_address"]
            if "formatted_address" in museumData
            else None
        )
        maps = museumData["url"] if "url" in museumData else None

        new_Museum = Museum(
            id=museumData["place_id"],
            name=dbArtwork.museum_name,
            image=photo_url,
            status=stat,
            address=addr,
            phone=phoneNum,
            rating=ratingInfo,
            website=site,
            maps_url=maps,
            hours=hoursList,
        )

        print(dbArtwork.museum_name)
        museumList.append(new_Museum)
        museumDict[dbArtwork.museum_name] = new_Museum

    museumObject = museumDict[dbArtwork.museum_name]
    if dbArtwork.museum == None:
        dbArtwork.museum = museumObject
    if museumObject not in dbArtwork.artist.museums:
        dbArtwork.artist.museums.append(museumObject)

db.session.add_all(museumList)
db.session.commit()
