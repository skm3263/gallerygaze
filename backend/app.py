# The general formatting and style of this file was based off of Caitlin's project: https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/master/backend/main.py

from re import search
from flask import Flask, jsonify, request
from sqlalchemy import or_, cast, asc, desc
import sqlalchemy.sql.sqltypes as types
from flask_marshmallow import Marshmallow
from dotenv import load_dotenv
import os
import flask
import json
import math

from sqlalchemy.sql.type_api import VisitableCheckKWArg
from models import *
from otherOptions import otherDict
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
load_dotenv()
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")


ma = Marshmallow(app)

# Schemas for model data as well as detailed instance data with relationships


class ArtworkSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Artwork


class ArtistSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Artist


class MuseumSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Museum


class MovementSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Movement


class SingleArtistSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Artist

    artworks = ma.Nested(ArtworkSchema, many=True)
    museums = ma.Nested(MuseumSchema, many=True)


class SingleArtworkSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Artwork

    artist = ma.Nested(ArtistSchema)
    museum = ma.Nested(MuseumSchema)
    movement = ma.Nested(MovementSchema)


class SingleMuseumSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Museum

    artworks = ma.Nested(ArtworkSchema, many=True)
    artists = ma.Nested(ArtistSchema, many=True)


class SingleMovementSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Movement

    artworks = ma.Nested(ArtworkSchema, many=True)


artists_schema = ArtistSchema(many=True)
artworks_schema = ArtworkSchema(many=True)
museums_schema = MuseumSchema(many=True)
movements_schema = MovementSchema(many=True)

single_artist_schema = SingleArtistSchema()
single_artwork_schema = SingleArtworkSchema()
single_museum_schema = SingleMuseumSchema()
single_movement_schema = SingleMovementSchema()


@app.route("/")
def hello_world():
    return '<img src="https://dazedimg-dazedgroup.netdna-ssl.com/967/azure/dazed-prod/1290/3/1293495.jpeg" alt="welcome" />'


# Returns all artists with pagination for Artist model
@app.route("/artists", methods=["GET"])
def get_artists():
    artists = Artist.query
    output, info = pagination_handler(request.args.to_dict(), artists, artists_schema)
    total = artists.count()
    return jsonify({"artists": output, "total": total, **info})


# Returns an artist instance with relationships to other models
@app.route("/artists/<id>", methods=["GET"])
def get_artist_id(id):
    artist = Artist.query.get(id)
    if artist is None:
        # return error response
        response = flask.Response(
            json.dumps({"error": "Artist with id '" + id + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_artist_schema.jsonify(artist)


# Returns all artworks with pagination for Artwork model
@app.route("/artworks", methods=["GET"])
def get_artworks():
    artworks = Artwork.query
    output, info = pagination_handler(request.args.to_dict(), artworks, artworks_schema)
    total = artworks.count()
    return jsonify({"artworks": output, "total": total, **info})


# Returns an artwork instance with relationships to other models
@app.route("/artworks/<id>", methods=["GET"])
def get_artwork_id(id):
    artwork = Artwork.query.get(id)
    if artwork is None:
        # return error response
        response = flask.Response(
            json.dumps({"error": "Artwork with id '" + id + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_artwork_schema.jsonify(artwork)


# Returns all museums with pagination for Museum model
@app.route("/museums", methods=["GET"])
def get_museums():
    museums = Museum.query
    output, info = pagination_handler(request.args.to_dict(), museums, museums_schema)
    total = museums.count()
    return jsonify({"museums": output, "total": total, **info})


# Returns a museum instance with relationships to other models
@app.route("/museums/<id>", methods=["GET"])
def get_museum_id(id):
    museum = Museum.query.get(id)
    if museum is None:
        # return error response
        response = flask.Response(
            json.dumps({"error": "Museum with id '" + id + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_museum_schema.jsonify(museum)


# Returns all movements with pagination for Movement model
@app.route("/movements", methods=["GET"])
def get_movements():
    movements = Movement.query
    output, info = pagination_handler(
        request.args.to_dict(), movements, movements_schema
    )
    total = movements.count()
    return jsonify({"movements": output, "total": total, **info})


# Returns a movement instance with relationships to other models
@app.route("/movements/<id>", methods=["GET"])
def get_movement_id(id):
    movement = Movement.query.get(id)
    if movement is None:
        # return error response
        response = flask.Response(
            json.dumps({"error": "Movement with id '" + id + "' not found"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_movement_schema.jsonify(movement)


# A search endpoint for handling all filtering, sorting, searching, etc.
@app.route("/search", methods=["GET"])
def query_handler():
    params = request.args.to_dict()
    model, schema = model_handler(params)
    queryInstances = model.query if model else None

    # Calls helper methods to search, filter, sort, and paginate data based on request
    if "general_string" in params:
        # handle string search
        if queryInstances:
            # searching in specific model
            queryInstances = search_handler(
                queryInstances, params["general_string"], model.__table__.columns
            )
        else:
            # general website search
            return jsonify(full_search_handler(params["general_string"]))

    if "filter" in params:
        # handle filtering
        queryInstances = filter_handler(
            queryInstances, params["filter"].split(";"), model
        )

    if "sort" in params:
        # handle sorting
        sortReverse = (
            params["sortReverse"] == "true" if "sortReverse" in params else False
        )
        queryInstances = sort_handler(queryInstances, params["sort"], sortReverse)

    output, info = pagination_handler(params, queryInstances, schema)
    return jsonify({"query results": output, "total": queryInstances.count(), **info})


# Sets the model and schema to filter/sort by based on API request parameters
# Returns None if no model was specified (i.e. for a general search)
def model_handler(params):
    # return model name and schema
    chosenModel = params["model"] if "model" in params else "all"
    if chosenModel == "artist":
        return (Artist, artists_schema)
    elif chosenModel == "artwork":
        return (Artwork, artworks_schema)
    elif chosenModel == "museum":
        return (Museum, museums_schema)
    elif chosenModel == "movement":
        return (Movement, movements_schema)
    else:
        return (None, None)


# Searches for instances with the searchString in any of their attributes
def search_handler(queryInstances, searchString, columns):
    condition = [
        cast(column, types.String).ilike(f"%{searchString}%") for column in columns
    ]
    return queryInstances.filter(or_(*condition))


# Searches for instances with the searchString in their attributes over all models
# Does not offer pagination but this was not required
def full_search_handler(searchString):
    models = [Artist, Artwork, Museum, Movement]
    schemas = [artists_schema, artworks_schema, museums_schema, movements_schema]
    names = ["artists", "artworks", "museums", "movements"]
    instances = {}
    for model, schema, name in zip(models, schemas, names):
        condition = [
            cast(column, types.String).ilike(f"%{searchString}%")
            for column in model.__table__.columns
        ]
        modelInstances = model.query.filter(or_(*condition))
        instances[name] = schema.dump(modelInstances)
    return instances


# Splits the filter parameter by each attribute
# Filters query by an AND across attribute but an OR across selected attribute values
def filter_handler(queryInstances, filterList, model):
    for filter in filterList:
        condition = []
        attribute, values = filter.split("=")
        column = getattr(model, attribute)
        for value in values.split(","):
            condition.extend(attribute_handler(column, value))
        queryInstances = queryInstances.filter(or_(*condition))
    return queryInstances


# Filters a column by a specified value
def attribute_handler(column, value):
    attribute = column.name

    # Filters names by beginning letters (e.g. A-E)
    if attribute == "name":
        startChar, endChar = value.split("-")
        return [
            column.startswith(chr(c))
            for c in range(ord(startChar.upper()), ord(endChar.upper()) + 1)
        ]

    # Filters dates by century by only considering first two digits
    elif attribute == "birthday" or attribute == "date_created":
        return [column.startswith(value[0:2])]

    # Filters numeric attributes depening on input
    # Filters by between, greater than or equal to, or just equal to
    elif attribute == "popularity_ranking" or attribute == "artwork_count":
        if "-" in value:
            startVal, endVal = value.split("-")
            return [column.between(int(startVal), int(endVal))]
        elif "+" in value:
            startVal = value.split("+")[0]
            return [column >= int(startVal)]
        else:
            return [column == int(value)]

    # In cases where 'other' is selected, filters by all values grouped into 'other'
    elif value.lower() == "other":
        otherList = otherDict[attribute]
        return [column.ilike(f"%{val}%") for val in otherList]

    # In all other cases checks that strings are equal
    else:
        if attribute == "media":
            return [column.ilike(f"%{value}%")]
        return [column.ilike(value)]


# Sorts a column in descending order if sortReverse is set to true, otherwise sorts in ascending order
def sort_handler(queryInstances, sortedAttribute, sortReverse):
    return (
        queryInstances.order_by(desc(sortedAttribute))
        if sortReverse
        else queryInstances.order_by(asc(sortedAttribute))
    )


# Paginates a group of queries based on parameters
# Returns start/end instance on current page as well as total pages
def pagination_handler(params, queryInstances, schema):
    total = queryInstances.count()

    # get values from params
    perPage = int(params["maxResults"]) if "maxResults" in params else 50
    pageNum = int(params["page"]) if "page" in params else 1

    # restrict pageNum from 1 to totalPages
    totalPages = math.ceil(total / perPage)
    pageNum = 1 if pageNum < 1 else totalPages if pageNum > totalPages else pageNum

    # put pagination info in dictionary
    start = perPage * (pageNum - 1) + 1 if pageNum > 0 else 0
    end = start + perPage - 1
    end = total if end > total else end
    paginationInfo = {"start": start, "end": end, "totalPages": totalPages}

    pageInstances = queryInstances.paginate(pageNum, perPage, False)
    return schema.dump(pageInstances.items), paginationInfo


def setup_app(app):
    db.init_app(app)


setup_app(app)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
