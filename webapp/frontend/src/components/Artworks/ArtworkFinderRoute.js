import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import ArtworkTable from "./ArtworkTable";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

function ArtworkFinderRoute() {
  useEffect(() => {
    document.title = "Gallery Gaze - Artworks"
  }, [])

  return (
    <MainWrapper>
      <h1>Artwork</h1>   
      <ArtworkTable/>
    </MainWrapper>
  );
}

export default ArtworkFinderRoute;
