import React, {useEffect} from 'react';
import styled from 'styled-components';
import MuseumTable from "./MuseumTable";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

function MuseumFinderRoute() {

  useEffect(() => {
    document.title = "Gallery Gaze - Museums"
  }, [])

  return (
    <MainWrapper>
      <h1>Museums</h1>
      <MuseumTable/>
    </MainWrapper>
  );
}

export default MuseumFinderRoute;
