import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import GlobalTable from "./GlobalTable";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

function GlobalFinderRoute(props) {

  useEffect(() => {
    document.title = "Gallery Gaze - Search"
  }, [])

  return (
    <MainWrapper>
      <h1>Search Results</h1>
      <GlobalTable searchValSent={props.location.state?.searchTerm ?? ''}/>
    </MainWrapper>
  );
}

export default GlobalFinderRoute;
