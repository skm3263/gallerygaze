import React, {useEffect} from 'react';
import {useHistory} from "react-router-dom"
import styled from 'styled-components';
import MediaCard from "./reuseable/MediaCard";

import leonardImage from "../media/leonardo-preview.jpg"
import artworkPreview from "../media/artwork-preview.jpg"
import museumPreview from "../media/museum-preview.jpg"
import SearchBar from "./reuseable/SearchBar";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const LandingIntro = styled.div`
  margin: auto;
  text-align: center;
  h1 {
    font-size: 5rem;
  }
`

const SectionNavigator = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`

const SearchWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`

const HomeRouteRoute = () => {

  const history = useHistory();

  useEffect(() => {
    document.title = "Gallery Gaze"
  }, [])

  const globalSearch = (searchVal) => {
    history.push({pathname: "/search", state:{searchTerm: searchVal}});
  }

  return (
    <MainWrapper>
      <LandingIntro>
        <h1>Gallery Gaze</h1>
        <p>Search through your favorite artwork, find your favorite artists, go to your favorite museums!</p>
      </LandingIntro>
      <SearchWrapper>
        <SearchBar onSearch={globalSearch}/>
      </SearchWrapper>
      <SectionNavigator>
        <MediaCard name="Artworks" image={artworkPreview} desc="Expore artwork!" clickAction={()=>history.push("/artworks")}/>
        <MediaCard name="Artists" image={leonardImage} desc="Expore artists from around the world!" clickAction={()=>history.push("/artists")}/>
        <MediaCard name="Museums" image={museumPreview} desc="Discover museuems near you" clickAction={()=>history.push("/museums")}/>
        <MediaCard name="Movements" image={'https://opinionstage-res.cloudinary.com/image/upload/c_lfill,dpr_3.0,f_auto,fl_lossy,q_auto:good,w_400/v1/polls/lcdgwbccxdokiuew8k6p'} desc="See all Movements!" clickAction={()=>history.push("/movements")}/>
      </SectionNavigator>
    </MainWrapper>
  );
}

export default HomeRouteRoute;
