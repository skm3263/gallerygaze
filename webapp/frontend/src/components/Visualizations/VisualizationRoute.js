import React, { useEffect } from 'react';
import styled from 'styled-components';

import CountryPopularityBarChart from './CountryPopularityBarChart';
import { NationalityGenderPieChart } from "./NationalityGenderPieChart";
import EraArtworksRadarChart from './EraArtworksRadarChart';
import RegionMedalBarChart from "./RegionMedalBarChart";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

const TitleWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 1rem;
  margin: 1rem 0;
  background-color: lightgray;
  font-weight: bold;
`;

const VisualWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const VisualizationRoute = () => {
  useEffect(() => {
    document.title = 'Gallery Gaze - Artworks';
  }, []);

  return (
    <MainWrapper>
      <TitleWrapper>
        <h1>Gallery Gaze Visualizations</h1>
      </TitleWrapper>
      <VisualWrapper>
        <h4> Top 20 Countries vs. Cumulative Ranking (lower is better) </h4>
        <CountryPopularityBarChart />
        <h4>Nationality vs. Gender</h4>
        <NationalityGenderPieChart />
        <h4>Artworks Per Era</h4>
        <EraArtworksRadarChart/>
        <br />
      </VisualWrapper>
    </MainWrapper>
  );
};

export default VisualizationRoute;
