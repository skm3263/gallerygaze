import {render, fireEvent, screen} from '@testing-library/react'
import { BrowserRouter as Router } from "react-router-dom";
import { act } from 'react-dom/test-utils';
import App from './App';
import HomeRoute from './components/HomeRoute'
import AboutRoute from './components/AboutRoute'
import Footer from './components/Footer'
import Navbar from './components/Navbar'
import MuseumTable from './components/Museums/MuseumTable'
import FilterSelect from './components/reuseable/FilterSelect'

/*
CITATION:

Some code for this testing suite was inspired by the WhereArtThou project from
Spring 2021, 10 AM. Thank you to them and their awesome work for helping
us put this group of unit tests together.
Link to their repo: https://gitlab.com/cs373-group-5/where-art-thou/-/tree/master
*/

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

test('renders HomeRoute without crashing', () => {
  render(
      <Router>
        <HomeRoute />
      </Router>
  );
});

test('Artworks button changes route', () => {
  render(
      <Router>
        <HomeRoute />
      </Router>
  );
  fireEvent.click(screen.getAllByText("Artworks")[0])
  expect(mockHistoryPush).toHaveBeenCalledWith('/artworks')
});

test('Artists button changes route', () => {
  render(
      <Router>
        <HomeRoute />
      </Router>
  );
  fireEvent.click(screen.getAllByText("Artists")[0])
  expect(mockHistoryPush).toHaveBeenCalledWith('/artists')
});

test('Museums button changes route', () => {
  render(
      <Router>
        <HomeRoute />
      </Router>
  );
  fireEvent.click(screen.getAllByText("Museums")[0])
  expect(mockHistoryPush).toHaveBeenCalledWith('/museums')
});

test('renders Navbar without crashing', () => {
  render(
      <Router>
        <Navbar />
      </Router>
  );
});

test('renders Footer without crashing', () => {
  render(
      <Router>
        <Footer />
      </Router>
  );
});

test('Tests AboutRoute can render', async () => {
  render(
    <Router>
      <AboutRoute />
    </Router>
  );
});

test('Loads App and checks About page for text', async () => {
  render(<App/>);
  fireEvent.click(screen.getByText('About'));
  const items = await screen.findAllByText('Jack', {exact: false});
  expect(items).toHaveLength(2);
});

test('Loads AboutRoute and checks About page for pictures', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByRole('Img', {exact: false});
  expect(items).toHaveLength(5);
});

test('Loads AboutRoute and checks About page for links', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByRole('link', {exact: false});
  expect(items).toHaveLength(5);
});

test('Loads AboutRoute and checks About page for commits', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByText('Commits: ', {exact: false});
  expect(items).toHaveLength(5);
});

test('Loads AboutRoute and checks About page for team commits', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByText('Team Stats', {exact: false});
  expect(items).toHaveLength(1);
});

test('Loads AboutRoute and checks About page for issues', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByText('Issues: ', {exact: false});
  expect(items).toHaveLength(5);
});

test('Loads AboutRoute and checks About page for no specific role', async () => {
  render(<AboutRoute/>)
  const items = await screen.findAllByRole('', {exact: false});
  expect(items).toHaveLength(105);
});

test('Tests Artist route shows properly', async () => {
  render(
      <App/>
  );
  fireEvent.click(screen.getAllByText("Artists")[0])
  const items = await screen.findAllByText('Artists', {exact: false});
  expect(items).toHaveLength(3);
});

test('Tests Artist route shows properly, 2', async () => {
  render(
    <App/>
  );
  fireEvent.click(screen.getAllByText("Artists")[0]);
  const items = await screen.findAllByRole('button', {exact: false});
  expect(items).toHaveLength(2);
});

test('Tests Artwork route shows properly', async () => {
  render(
      <App/>
  );
  fireEvent.click(screen.getAllByText("Artwork")[0])
  const items = await screen.findAllByText('Artists', {exact: false});
  expect(items).toHaveLength(2);
});

test('Tests Artwork route shows properly, 2', async () => {
  render(
    <App/>
  );
  fireEvent.click(screen.getAllByText("Artwork")[0])
  const items = await screen.findAllByRole('button', {exact: false});
  expect(items).toHaveLength(2);
});

test('Tests Museums route shows properly', async () => {
  render(
      <App/>
  );
  fireEvent.click(screen.getAllByText("Museums")[0])
  const items = await screen.findAllByText('Museum', {exact: false});
  expect(items).toHaveLength(3);
});

test('Tests Museum table renders properly', async () => {
  act(() => {
    render(
      <MuseumTable/>
    );
  });
  const items = await screen.findAllByRole('columnheader', {exact: false});
  expect(items).toHaveLength(3);
});

test('Tests FilterSelect 1', async () => {

  const titles = ['test 1', 'test 2', 'title 3'];
  const options = 
  [
    ['opt1', '1 opt2'],
    ['Gallery', 'Gaze', 'Hmm'],
    ['These', 'are', 'testing', 'options that do', 'nothing']
  ]
  render(
    <FilterSelect
      filterTitles = {titles}
      modelFilters = {options}
      onClick = {() => {return 1}}
    />
  );
  const items1 = await screen.findAllByText('1 opt2', {exact: false});
  expect(items1).toHaveLength(1);
  const items2 = await screen.findAllByText('options that do', {exact: false});
  expect(items2).toHaveLength(1);
});

test('Tests Artwork page to have filtering and sorting', async () => {
  render(
    <App/>
  );
  fireEvent.click(screen.getAllByText("Artwork")[0]);

  const items1 = await screen.findAllByText('Name', {exact: false});
  expect(items1).toHaveLength(2);
  const items2 = await screen.findAllByText('1300s', {exact: false});
  expect(items2).toHaveLength(1);
});

test('Tests model page to have search', async () => {
  render(
    <App/>
  );
  fireEvent.click(screen.getAllByText("Museums")[0])
  const items = await screen.findAllByText('Search anything', {exact: false});
  expect(items).toHaveLength(1);
});

test('Tests FilterSelect 2', async () => {

  const titles = ['test 1', 'test 2', 'title 3'];
  const options = 
  [
    ['opt1', '1 opt2'],
    ['Gallery', 'Gaze', 'Hmm'],
    ['These', 'are', 'testing', 'options that do', 'nothing']
  ]
  render(
    <FilterSelect
      filterTitles = {titles}
      modelFilters = {options}
      onClick = {() => {return 1}}
    />
  );
  const items = await screen.findAllByRole('checkbox', {exact: false});
  expect(items).toHaveLength(10);
});

