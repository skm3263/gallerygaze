# The general formatting and style of this file was based off of Caitlin's project: https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/master/backend/tests.py

from unittest import main, TestCase
import requests

import os
from dotenv import load_dotenv

load_dotenv()
BRANCH_NAME = os.getenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME") or os.getenv(
    "CI_COMMIT_BRANCH"
)
MAIN_BRANCH_NAME = os.getenv("MAIN_BRANCH_NAME")
assert BRANCH_NAME is not None
assert MAIN_BRANCH_NAME is not None

PROTO = "https"
DOMAIN = "gallerygaze.me"
ENDPOINT = f"{PROTO}://api.{'' if BRANCH_NAME == MAIN_BRANCH_NAME else 'beta.'}{DOMAIN}"
print(f"ENDPOINT: {ENDPOINT}")


class Tests(TestCase):

    # ---------------------
    # Artist endpoint tests
    # ---------------------

    # Tests for expected number of artist instances
    def test_artists_count(self):
        artistsResponse = requests.get(f"{ENDPOINT}/artists")
        assert artistsResponse.status_code == 200
        data = artistsResponse.json()
        assert data["total"] == 143
        assert data["totalPages"] == 3
        assert data["start"] == 1
        assert data["end"] == 50

    # Tests a particular artist instance returned from an all artists query
    def test_artists_all(self):
        artistsResponse = requests.get(f"{ENDPOINT}/artists")
        assert artistsResponse.status_code == 200
        data = artistsResponse.json()
        assert data["artists"][0] == {
            "artwork_count": 8,
            "bio": "Considered the leader of the French Romantic school of painting, Eugene Delacroix was a prolific artist, producing over 9,000 works during his lifetime, ranging from paintings, to watercolors, pastels and drawings. His work both shaped the Impressionist artists and inspired the Symbolist movement. Taking inspiration from Peter Paul Rubens and the Venetian Renaissance painters, his painting style emphasized colors and movement rather than the carefully modeled form and clear outline. After completing his education in art, he submitted his first work the Paris Salon The Barque of the Dante, which was accepted, in 1822. This first piece caused a sensation and was harshly criticized, but it was nonetheless bough by the State for the Luxembourg Galleries. This began a pattern of his critical reception throughout his career. First, his work was criticized, but later accepted and purchased by the state or personal patron. \r\n\r\nHis works revolved around many themes, many of which were inspired by the works of Shakespeare, Goethe, and Byron, and entailed the motifs of violence and sensuality. Perhaps his most widely know piece is Liberty Leading the Republic, which invoked the image of liberty leading the people. The French government initially bough the painting, but soon deemed it too anti-establishment, and had it withdrawn from view. In 1832, he traveled to Morocco to escape Paris society, and experience a more primitive culture. His time there inspired over 100 paintings, and he compared the people there to the early citizens of Rome or Greece. \r\n\r\nPerhaps the most scandalous or noteworthy aspect of Delacroix\u2019s personal life was the fact that he was most likely the illegitimate child of Talleyrand, a French diplomat, as his father never conceived any other children. After both his father and mother died, leaving him an orphan at the age of 16, he spent much of life under the protection of the Frenchman. His legacy includes a number of portraits, landscapes, and figure paintings. He also helped found the Societe Nationale des Beaux-Arts, along with other notable French artists of the day. His face was featured on the French banknote, minted in 2003.  ",
            "birthday": "1798",
            "deathday": "1863",
            "gender": "Male",
            "hometown": "Charenton-Saint-Maurice, France",
            "id": "57726d7cedc2cb3880b47baa",
            "image": "https://uploads8.wikiart.org/images/eugene-delacroix.jpg!Portrait.jpg",
            "name": "Eugene Delacroix",
            "nationality": "French",
            "popularity_ranking": 23,
        }

    # Tests the more detailed response data for a specific artist based on id
    def test_artist_instance(self):
        artistResponse = requests.get(f"{ENDPOINT}/artists/57726d85edc2cb3880b48ccd")
        assert artistResponse.status_code == 200
        data = artistResponse.json()
        assert data["name"] == "Leonardo da Vinci"
        assert data["popularity_ranking"] == 3
        assert len(data["artworks"]) == 7
        assert len(data["museums"]) == 7
        assert data["birthday"] == "1452"
        assert data["deathday"] == "1519"
        assert data["hometown"] == "Anchiano, Italy"

    # Tests expected errors are handled for incorrect artist ids
    def test_artist_instance_error(self):
        artistResponse = requests.get(f"{ENDPOINT}/artists/123456789")
        assert artistResponse.status_code == 404
        data = artistResponse.json()
        assert data == {"error": "Artist with id '123456789' not found"}

    # ---------------------
    # Artwork endpoint tests
    # ---------------------

    # Tests for expected number of artwork instances
    def test_artworks_count(self):
        artworksResponse = requests.get(f"{ENDPOINT}/artworks")
        assert artworksResponse.status_code == 200
        data = artworksResponse.json()
        assert data["total"] == 527
        assert data["totalPages"] == 11
        assert data["start"] == 1
        assert data["end"] == 50

    # Tests a particular artwork instance returned from an all artworks query
    def test_artworks_all(self):
        artworksResponse = requests.get(f"{ENDPOINT}/artworks")
        assert artworksResponse.status_code == 200
        data = artworksResponse.json()
        assert data["artworks"][0] == {
            "date_created": "1509",
            "desc": 'This work is one of the mysteries of European painting: in spite of its undeniable quality and epochal importance, opinions are divergent concerning both its creator and its theme. It is the outstanding masterpiece of the Venetian Renaissance, the summit of Giorgione\'s creative career, so much so that according to some it may have been painted, or at least finished, by Titian rather than Giorgione.\r\n\r\nThe painting has been interpreted as an allegory of Nature, similar to Giorgione\'s Storm, which was undeniably painted by him; it was even viewed as the first example of the modern herdsman genre. Its message must be more complex than this. It is likely that the master consciously unified several themes in this painting, and the deciphering of symbols required a degree of erudition even at the time of its creation. During the eighteenth century the painting was known by the simple name of "Pastorale" and only subsequently was it given the title "F\u00eate champ\u00eatre" or "Concert champ\u00eatre", owing to its festive mood. Modern research has pointed out that the composition is in fact an allegory of poetry.\r\n\r\nThe female figures in the foreground are probably the Muses of poetry, their nakedness reveals their divine being. The standing figure pouring water from a glass jar represents the superior tragic poetry, while the seated one holding a flute is the Muse of the less prestigious comedy or pastoral poetry. The well-dressed youth who is playing a lute is the poet of exalted lyricism, while the bareheaded one is an ordinary lyricist. The painter based this differentiation on Aristotle\'s "Poetica".\r\n\r\nThe scenery is characterized by a duality. Between the elegant, slim trees on the left, we see a multi-levelled villa, while on the right, in a lush grove, we see a shepherd playing a bagpipe. Yet the effect is completely unified. The very presence of the beautiful, mature Muses provides inspiration; the harmony of scenery and figures, colours and forms proclaims the close interrelationship between man and nature, poetry and music.',
            "genre": "Allegorical Painting",
            "height": 570,
            "id": "57726e17edc2cb3880b5f298",
            "image": "https://uploads7.wikiart.org/pastoral-concert-f\u00eate-champ\u00eatre-1509(2).jpg!Large.jpg",
            "media": "Oil, Canvas",
            "movement_name": "High Renaissance",
            "name": "Pastoral Concert (F\u00eate champ\u00eatre)",
            "popularity_ranking": 337,
            "width": 750,
        }

    # Tests the more detailed response data for a specific artwork based on id
    def test_artwork_instance(self):
        artworkResponse = requests.get(f"{ENDPOINT}/artworks/5772716cedc2cb3880c1907f")
        assert artworkResponse.status_code == 200
        data = artworkResponse.json()
        assert data["name"] == "The Starry Night"
        assert data["popularity_ranking"] == 2
        assert data["width"] == 750
        assert data["height"] == 594
        assert data["date_created"] == "1889"

    # Tests expected errors are handled for incorrect artwork ids
    def test_artwork_instance_error(self):
        artworkResponse = requests.get(f"{ENDPOINT}/artworks/5772716cedc2cb3fdsc1907f")
        assert artworkResponse.status_code == 404
        data = artworkResponse.json()
        assert data == {"error": "Artwork with id '5772716cedc2cb3fdsc1907f' not found"}

    # ---------------------
    # Museum endpoint tests
    # ---------------------

    # Tests for expected number of museum instances
    def test_museums_count(self):
        museumsResponse = requests.get(f"{ENDPOINT}/museums")
        assert museumsResponse.status_code == 200
        data = museumsResponse.json()
        assert data["total"] == 192
        assert data["totalPages"] == 4
        assert data["start"] == 1
        assert data["end"] == 50

    # Tests a particular museum instance returned from an all museums query
    def test_museums_all(self):
        museumsResponse = requests.get(f"{ENDPOINT}/museums")
        assert museumsResponse.status_code == 200
        data = museumsResponse.json()
        assert data["museums"][0] == {
            "address": "746 96 Skokloster, Sweden",
            "artwork_count": 2,
            "city": "H\u00e5bo Municipality",
            "country": "Sweden",
            "hours": "Not Available",
            "id": "ChIJ3QwcS-G2X0YRIi4gPGr5w88",
            "image": "https://www.aronson.com/wp-content/uploads/2020/10/Skokloster-Castle-Sweden.png",
            "lat": 59.7031769,
            "lng": 17.6211558,
            "maps_url": "https://maps.google.com/?cid=14971083820980252194",
            "name": "Skokloster Castle",
            "phone": "+46 8 402 30 60",
            "popularity_ranking": 44,
            "rating": 4.4,
            "status": "Open",
            "website": "http://www.skoklostersslott.se/",
        }

    # Tests the more detailed response data for a specific museum based on id
    def test_museum_instance(self):
        museumResponse = requests.get(f"{ENDPOINT}/museums/ChIJKcGbg2NgLxMRthZkUqDs4M8")
        assert museumResponse.status_code == 200
        data = museumResponse.json()
        assert data["address"] == "00120, Vatican City"
        assert data["popularity_ranking"] == 1
        assert len(data["artworks"]) == 1
        assert len(data["artists"]) == 1
        assert data["name"] == "Vatican Museums"
        assert data["phone"] == "+39 06 6988 4676"
        assert data["rating"] == 4.6
        assert data["status"] == "Open"
        assert (
            data["website"]
            == "http://www.museivaticani.va/content/museivaticani/it.html"
        )

    # Tests expected errors are handled for incorrect museum ids
    def test_museum_instance_error(self):
        museumResponse = requests.get(f"{ENDPOINT}/museums/ChIJKcGuhuduUBBKio23")
        assert museumResponse.status_code == 404
        data = museumResponse.json()
        assert data == {"error": "Museum with id 'ChIJKcGuhuduUBBKio23' not found"}

    # ---------------------
    # Movement endpoint tests
    # ---------------------

    # Tests for expected number of movement instances
    def test_movements_count(self):
        movementsResponse = requests.get(f"{ENDPOINT}/movements")
        assert movementsResponse.status_code == 200
        data = movementsResponse.json()
        assert data["total"] == 49
        assert data["totalPages"] == 1
        assert data["start"] == 1
        assert data["end"] == 49

    # Tests a particular movement instance returned from an all movements query
    def test_movements_all(self):
        movementResponse = requests.get(f"{ENDPOINT}/movements")
        assert movementResponse.status_code == 200
        data = movementResponse.json()
        assert data["movements"][0] == {
            "artwork_count": 1,
            "desc": 'Analytical Cubism is the second period of the Cubism art movement that ran from 1910 to 1912. It was led by the "Gallery Cubists" Pablo Picasso and Georges Braque. This form of Cubism analyzed the use of rudimentary shapes and overlapping planes to depict the separate forms of the subjects in a painting. It refers to real objects in terms of identifiable details that become\u2014through repetitive use\u2014signs or clues that indicate the idea of the object. It is considered to be a more structured and monochromatic approach than that of Synthetic Cubism. This is the period that quickly followed and replaced it and was also developed by the artistic duo.',
            "id": "30",
            "image": "https://uploads8.wikiart.org/images/pablo-picasso/dance-of-the-veils-1907.jpg!Large.jpg",
            "name": "Analytical Cubism",
        }

    # Tests the more detailed response data for a specific movement based on id
    def test_movement_instance(self):
        movementResponse = requests.get(f"{ENDPOINT}/movements/37")
        assert movementResponse.status_code == 200
        data = movementResponse.json()
        assert data["name"] == "Cubism"
        assert data["artwork_count"] == 4

    # Tests expected errors are handled for incorrect movement ids
    def test_movement_instance_error(self):
        movementResponse = requests.get(f"{ENDPOINT}/movements/57")
        assert movementResponse.status_code == 404
        data = movementResponse.json()
        assert data == {"error": "Movement with id '57' not found"}

    # ---------------------
    # Query endpoint tests
    # ---------------------

    # Tests general search parameter
    def test_general_search(self):
        queryResponse = requests.get(f"{ENDPOINT}/search?general_string=color")
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        assert len(data) == 4
        assert len(data["artists"]) == 22
        assert "color" in data["artists"][0]["bio"]

    # Tests model specific search
    def test_model_search(self):
        queryResponse = requests.get(
            f"{ENDPOINT}/search?model=artwork&general_string=classic"
        )
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        assert data["total"] == 32
        assert "classic" in data["query results"][0]["desc"]

    # Tests sorting on an attribute
    def test_sort(self):
        queryResponse = requests.get(
            f"{ENDPOINT}/search?model=artist&sort=popularity_ranking"
        )
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        results = data["query results"]
        assert results[0]["popularity_ranking"] == 1
        assert results[1]["popularity_ranking"] == 2
        assert results[2]["popularity_ranking"] == 3
        assert results[3]["popularity_ranking"] == 4

    # Tests filtering on an attribute
    def test_filter(self):
        queryResponse = requests.get(
            f"{ENDPOINT}/search?model=museum&filter=status=open;country=france;city=paris"
        )
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        results = data["query results"]
        assert results[0]["status"] == "Open"
        assert results[0]["city"] == "Paris"
        assert results[0]["country"] == "France"
        assert results[1]["status"] == "Open"
        assert results[1]["city"] == "Paris"
        assert results[1]["country"] == "France"

    # Tests pagination
    def test_pagination(self):
        queryResponse = requests.get(
            f"{ENDPOINT}/search?model=movement&maxResults=5&page=3"
        )
        assert queryResponse.status_code == 200
        data = queryResponse.json()
        assert data["total"] == 49
        assert data["totalPages"] == 10
        assert data["start"] == 11
        assert data["end"] == 15


if __name__ == "__main__":
    main()
