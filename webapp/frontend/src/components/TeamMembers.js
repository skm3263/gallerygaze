import colepic from "../media/colekauppinen.png";
import jackpic from "../media/jackgreer.png";
import lancepic from "../media/lancenguyen.png";
import shanpic from "../media/shanmemon.png";
import sohampic from "../media/sohamroy.png";

/* CITATION: 
     The code to attribute the stats (commits and issues) from the GitLab API to each
     member is inspired by and derived from Spring '21 11am group readyrecipes. repo:
     https://gitlab.com/cs373-group13/readyrecipes/-/tree/master 
     The design of the About page and our members is our own */

const TeamMembers = [

    {
        name : "Cole Kauppinen",
        email : "kauppinencole@gmail.com",
        commits: 0,
        issues: 0,
        photo: colepic,
        bio: "Hey! My name is Cole Kauppinen and I'm a Junior studying Computer Science at UT. Outside of school I like to read, play basketball, and sample music.",
        job: "Back-end/API designer",
        gitlabid: "kauppcol"
    },
    {
        name : "Jack Greer",
        email : "jackgreer79@gmail.com",
        commits: 0,
        issues: 0,
        photo: jackpic,
        bio: "I'm Jack, a junior here at UT Austin. I'm a big fan of college football, go horns! I love brewing coffee, listening to music, and hanging out with friends.",
        job: "Front-end/React guy",
        gitlabid: "jackgreer79"
    },
    {
        name : "Lance Nguyen",
        email : "lancenguyen02@utexas.edu",
        commits: 0,
        issues: 0,
        photo: lancepic,
        bio: "Hey, my name is Lance! I’m a senior studying Computer Science at UT. Besides programming, I’m really into music, workout out, and spending time with family and friends.",
        job: "Back-end/API designer",
        gitlabid: "lancenguyen02"
    },
    {
        name : "Shan Memon",
        email : "shankmemon@gmail.com",
        commits: 0,
        issues: 0,
        photo: shanpic,
        bio: "Hi! My name is Shan Memon and i’m a Junior studying Computer Science and Business at UT. I like to play and make video games. I played on UT’s CS:GO JV team freshman year!",
        job: "Front-end/React guy",
        gitlabid: "skm3263"
    },
    {
        name : "Soham Roy",
        email : "sohamroy@sohamroy.me",
        commits: 0,
        issues: 0,
        photo: sohampic,
        bio: "I'm a 3rd year student studying Computer Science at UT. When I'm not programming I like playing tennis and going hiking. I like to spend my summers staring at screens frustrated.",
        job: "Web management/Docker pro",
        gitlabid: "Soham3-1415"
    },
]

export default TeamMembers
