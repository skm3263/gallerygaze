import React, { useEffect, useState } from 'react';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer } from 'recharts';

const BLUE = '#8cd3ff';
const PINK = 'ffc0cb';

export const NationalityGenderPieChart = () => {
  const [nationalityData, setNationalityData] = useState([]);
  const [genderData, setGenderData] = useState([]);

  useEffect(() => {
    const query =
      'https://api.beta.gallerygaze.me/search?model=artist&page=1&maxResults=500';
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((data) => {
        const nationalites = {};
        const gender = {};
        for (let artistIndex in data['query results']) {
          let artist = data['query results'][artistIndex];
          if (artist.nationality in nationalites) {
            nationalites[artist.nationality]++;
          } else {
            nationalites[artist.nationality] = 1;
          }

          const isMale = artist.gender === 'Male' ? 1 : 0;
          const isFemale = artist.gender === 'Female' ? 1 : 0;
          if (artist.nationality in gender) {
            gender[artist.nationality].males += isMale;
            gender[artist.nationality].females += isFemale;
          } else {
            gender[artist.nationality] = { males: isMale, females: isFemale };
          }
        }
        let newData = [];
        for (let nationality in nationalites) {
          newData = [
            ...newData,
            { name: nationality, value: nationalites[nationality] },
          ];
        }
        setNationalityData(newData);
        newData = [];
        for (let g in gender) {
          const total = gender[g].females + gender[g].males;
          newData = [
            ...newData,
            {
              name: `${g}-males`,
              value: (gender[g].males / total) * nationalites[g],
              fill: BLUE,
            },
            {
              name: `${g}-females`,
              value: (gender[g].females / total) * nationalites[g],
              fill: 'pink',
            },
          ];
        }
        setGenderData([{ data: newData }]);
        console.log([{ data: newData }]);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const data01 = [
    { name: 'Group A', value: 400 },
    { name: 'Group B', value: 300 },
    { name: 'Group C', value: 300 },
    { name: 'Group D', value: 200 },
  ];
  const data02 = [
    { name: 'A1', value: 100 },
    { name: 'A2', value: 300 },
    { name: 'B1', value: 100 },
    { name: 'B2', value: 80 },
    { name: 'B3', value: 40 },
    { name: 'B4', value: 30 },
    { name: 'B5', value: 50 },
    { name: 'C1', value: 100 },
    { name: 'C2', value: 200 },
    { name: 'D1', value: 150 },
    { name: 'D2', value: 50 },
  ];

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = (props) => {
    const { cx, cy, midAngle, innerRadius, outerRadius, percent, index } =
      props;

    const radius = innerRadius + (outerRadius - innerRadius) * 1.1;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <>
        <text
          x={x}
          y={y}
          fill='black'
          textAnchor={x > cx ? 'start' : 'end'}
          dominantBaseline='central'
        >
          {props.name}
        </text>
      </>
    );
  };

  return (
    <ResponsiveContainer width='100%' height={550}>
      <PieChart>
        {genderData.map((s) => (
          <Pie
            data={s.data}
            dataKey='value'
            cx='50%'
            cy='50%'
            innerRadius={'75%'}
            outerRadius={'100%'}
            fill='#000'
            labelLine={false}
            label
            key={1}
          />
        ))}

        <Pie
          data={nationalityData}
          dataKey='value'
          cx='50%'
          cy='50%'
          outerRadius={'60%'}
          fill='#8884d8'
          labelLine={false}
          label={renderCustomizedLabel}
        />
      </PieChart>
    </ResponsiveContainer>
  );
};
