import React from 'react'
import styled from 'styled-components';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';

const MainWrapper = styled.div`
  display: flex;
  justify-content: center;
`

let containerStyle = {
  width: '400px',
  height: '400px'
};

const Map = (props) => {
  const {center, width, height} = props;

  const size = {width: width ?? containerStyle.width, height: height ?? containerStyle.height }

  return (
    <MainWrapper>
      <LoadScript
        googleMapsApiKey={"AIzaSyAfnhqDQLEVQrcsTVrBR14XqlFYUDOcuk0"}
      >
        <GoogleMap
          mapContainerStyle={size}
          center={center}
          zoom={15}
        >
          { <Marker position={center} /> }
          <></>
        </GoogleMap>
      </LoadScript>
    </MainWrapper>
  )
}

export default Map
