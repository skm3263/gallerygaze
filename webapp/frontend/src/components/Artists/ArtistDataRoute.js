import React, {useState, useEffect} from 'react'
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import artistData from '../../data/artists.json';
import { useParams } from 'react-router-dom';
import Paper from '@mui/material/Paper';
import ArtworkCarousel from "../Artworks/ArtworkCarousel";
import MuseumCarousel from "../Museums/MuseumCarousel";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

const MiddleSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`

const LeftSide = styled.div`
  width: 30%;
  `
const RightSide = styled.div`
  width: 50%; 
`

const Carousels = styled.div`
  display: flex;
  justify-content: center;
`

const RedirectLink = styled(Link)`
  text-decoration: none;
  color: black;
`;
 
const getData = (id) => artistData.find(e => e.artist_id == id)

const ArtistDataRoute = (props) => {
  const { id } = useParams();
  const [data, setData] = useState({});
  const searchVal = props.location.state?.searchTerm ?? '';

  // highlight text that matches search
  const highlightText = (text) => {
    try {
      const textStr = String(text);
      // Split text on highlight term, include term itself into parts, ignore case
      const parts = textStr.split(new RegExp(`(${searchVal})`, 'gi'));
      return <span>{parts.map(part => part.toLowerCase() === searchVal.toLowerCase() ? <mark>{part}</mark> : part)}</span>;
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetch("https://api.beta.gallerygaze.me/artists/" + id)
    .then(response => {
      if (response.ok)
        return response.json();
      throw response
    })
    .then(data => {
      console.log(data)
      setData(data)
    })
    .catch(err => console.error("ERROR FETCH: ", err))
  }, [])

  return (
    <MainWrapper>
      <h1>{highlightText(data.name)}</h1>
      <MiddleSection>
        <LeftSide>
          <img src={data.image} width="500px" alt={data.name}/>
        </LeftSide>
        <RightSide>
          <Paper sx={{padding: '2rem'}}>
            <p>{highlightText(data.bio)}</p> 
            <p> <b>Date of Birth:</b> {highlightText(data.birthday)} </p>
            <p> <b>Date of Death:</b> {highlightText(data.deathday)}</p>
            <p> <b>Birthplace:</b> {highlightText(data.hometown)}</p>
            <p> <b>Nationality:</b> {highlightText(data.nationality)}</p> 
            <p> <b>Gender:</b> {highlightText(data.gender)}</p> 
            <p> <b>Popularity Ranking:</b> {highlightText(data.popularity_ranking)}</p> 
            <p> <b>Number of artworks:</b> {highlightText(data.artwork_count)}</p> 
            {/* <p> <RedirectLink to={'/artworks/' + data.artwork_id}> <b>Famous work:</b> <font color= "#C58917"> {data.famous_artwork}</font> </RedirectLink></p>
            <p> <RedirectLink to={'/museums/' + data.museum_id}> <b>Main locations:</b> <font color="#DC143C"> {data.museum_name}</font> </RedirectLink></p>             */}
          </Paper>
        </RightSide>
      </MiddleSection>
      <Carousels>
        {data && data.artworks && <ArtworkCarousel data={data.artworks}/>}
        {data && data.museums && <MuseumCarousel data={data.museums}/>}
      </Carousels>
    </MainWrapper>
  );
}

export default ArtistDataRoute;
