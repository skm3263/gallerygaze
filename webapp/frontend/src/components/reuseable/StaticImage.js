import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';

const MediaCard = (props) => {
  const {name, image} = props

  return (
    <Card sx={{ width: 350, margin: "1rem" }}>
    <CardMedia
        component="img"
        height="400"
        image={image}
        alt={name}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {name}
          </Typography>
    </CardContent>
    </Card>
  )
}

export default MediaCard