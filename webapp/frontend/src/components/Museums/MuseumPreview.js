import React from 'react'
import styled from "styled-components"

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  cursor: pointer;
`

const MuseumPreview = (props) => {
  const {data, onClick} = props;
  return (
    <MainWrapper onClick={onClick}>
      <h2>Location</h2>
      {data && data.name && <h3>{data.name}</h3>}
      {data && data.image && <img src={data.image} height='150px' alt={data.name}/>}
    </MainWrapper>
  )
}

export default MuseumPreview
