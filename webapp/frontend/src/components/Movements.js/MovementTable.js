import React, {useState, useEffect} from 'react'
import styled from 'styled-components';
import { useHistory } from "react-router";
import { makeStyles } from "@mui/styles";
import { DataGrid } from '@mui/x-data-grid';
import SearchBar from "../reuseable/SearchBar";
import FilterSelect from "../reuseable/FilterSelect";


const useStyles = makeStyles({
  root: {
    '& .MuiDataGrid-columnHeaderTitle': {
      fontWeight: 'bold',
    },
  },
});

const TableWrapper = styled.div`
  width: '100%';
  font-weight: 'bold';
  margin: 2rem 0;
`;

const SearchWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`

const MovementTable = props => {
  const classes = useStyles();
  const history = useHistory();

  const [data, setData] = useState({entries:[], total: 0});
  const [pageNum, setPageNum] = useState(0);
  const [searchVal, setSearchVal] = useState("");
  const [instancesPerPage, setInstancesPerPage] = useState(25);
  const [selectedFilterState, setSelectedFilterState] = useState({});
  const [filterParamString, setFilterParamString] = useState("");
  const [sortParamString, setSortParamString] = useState("");

  useEffect(() => {
    updateTableData(pageNum, instancesPerPage);
  }, [pageNum, instancesPerPage, searchVal, selectedFilterState, filterParamString, sortParamString]);

  // highlight text that matches search
  const highlightText = (text) => {
    try {
      let reg = new RegExp(`(${searchVal})`, 'gi');
      // Split text on highlight term, include term itself into parts, ignore case
      const parts = text.split(reg);
      return <span>{parts.map(part => part.toLowerCase() === searchVal.toLowerCase() ? <mark>{part}</mark> : part)}</span>;
    } catch (error) {
      console.log(error);
    }
  };
  
  const museumFilterTitles = [
    "Number of Artworks"
  ];

  const museumFilters = [
    [
      "1", "2", "3", "4", "5", "6+"
    ]
  ];

  const filterMapping = {
    "number_of_artworks": "artwork_count"
  }

  const mapFilterValues = (filter) => {
    if (filter in filterMapping) {
      return filterMapping[filter];
    }
    return filter;
  }

  const onCheckboxClick = (title, value) => {
    let newState = selectedFilterState;

    if (title in newState) {
      if (newState[title].includes(value)) {
        newState[title] = newState[title].filter(item => item !== value);
        if (newState[title].length === 0) {
          delete newState[title];
        }
      } else {
        newState[title] = [...newState[title], value];
      }
    } else {
      newState[title] = [value];
    }
    let filterParams = `&filter=`;
    for (let key in newState) {
      const newKey = String(key).replaceAll(" ", "_");
      filterParams += `${mapFilterValues(String(newKey).toLowerCase())}=${newState[key].map(e => mapFilterValues(e)).join(",").toLowerCase()};`;
    }
    // get rid of last ;
    filterParams = filterParams.slice(0, -1);

    if (filterParams !== "&filter=") {
      setFilterParamString(filterParams.replace("+", "%2B"));
    } else {
      setFilterParamString("");
    }

    setSelectedFilterState(newState);
  }

  const sortByColumn = (e) => {
    if (e.length <= 0) return;

    let sortParam = `&sort=${e[0].field}&sortReverse=${e[0].sort === 'asc' ? 'false' : 'true'}`;
    setSortParamString(sortParam);
  }

  const columns = [
    { field: 'name', headerName: 'Name', renderCell: (params)=> (
      highlightText(String(params.value))
    ), flex: 1},
    { field: 'artwork_count', headerName: 'Artwork Count', renderCell: (params)=> (
      highlightText(String(params.value))
    ), flex: 1},
  ];

  const handleSearch = (searchValue) => {
    console.log("searching for " + searchValue);
    setSearchVal(searchValue);
    setPageNum(0);
  };

  const updateTableData = (page, pageSize) => {
    console.log("Updating table")
    const newPageNum = page + 1;
    let query =`https://api.beta.gallerygaze.me/search?model=movement${searchVal !== '' ? `&general_string=${searchVal}` : ''}&page=${newPageNum}&maxResults=${pageSize}`;

    query += filterParamString;
    query += sortParamString;
    console.log(String(query));

    // remove &filter if there are none selected
    const endVal = query.length;
    if (query.slice(endVal - 7, endVal) === '&filter') {
      query = query.slice(0, endVal - 7);
    }

    console.log(query);
    fetch(query)
    .then((response) => {
      if (response.ok) return response.json();
      throw response;
    })
    .then((res) => {
      console.log(res);
      res.entries = res["query results"];
      setData(res);
    })
    .catch((err) => console.error('ERROR FETCH: ', err));
  };

  return (
    <TableWrapper className={classes.root}>
      <FilterSelect
        filterTitles = {museumFilterTitles}
        modelFilters = {museumFilters}
        onClick={onCheckboxClick}
      />
      <SearchWrapper>
        <SearchBar onSearch={handleSearch}/>
      </SearchWrapper>

      <DataGrid 
        pagination
        paginationMode = "server"
        rowCount={data.total}
        onPageChange={setPageNum}
        pageSize={instancesPerPage}
        onPageSizeChange={setInstancesPerPage}
        autoHeight={true} 
        rows={data.entries} 
        columns={columns} 
        disableSelectionOnClick={true} 
        onRowClick={(row) => history.push({pathname: '/movements/' + row.id, state:{searchTerm: searchVal}})}
        onSortModelChange={sortByColumn}
        />
    </TableWrapper>
  )
}

export default MovementTable;

