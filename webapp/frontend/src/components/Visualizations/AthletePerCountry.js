import React, { useEffect, useState } from 'react';
import {
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
  ResponsiveContainer,
} from 'recharts';

const AthletePerCountry = () => {
  const data1 = [
    {
      subject: 'Math',
      A: 120,
    },
    {
      subject: 'Chinese',
      A: 98,
    },
    {
      subject: 'English',
      A: 86,
    },
    {
      subject: 'Geography',
      A: 99,
    },
    {
      subject: 'Physics',
      A: 85,
    },
    {
      subject: 'History',
      A: 65,
    },
  ];

  const [data, setData] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const query = `https://api.goingforgold.me/api/athletes?p=-1`;
      let response = await fetch(query);
      let json = await response.json();
      let dictionary = {};
      for (let entry in json['body']) {
        const country = json['body'][entry]["country_name"];
        if (!dictionary[country]) {
          dictionary[country] = 1;
        } else {
          dictionary[country]++;
        }
      }
      let newData = [];
      for (let entry in dictionary) {
        newData.push({
          country: entry,
          amount: dictionary[entry],
        });
      }
      // only keep top 15 countries
      newData.sort((a, b) => b.amount - a.amount);
      newData = newData.slice(0, 15);

      setData(newData);
    }
    fetchData();
  }, []);

  return (
    <ResponsiveContainer width='100%' height={500}>
      <RadarChart cx='50%' cy='50%' outerRadius='80%' data={data}>
        <PolarGrid />
        <PolarAngleAxis dataKey='country' />
        <PolarRadiusAxis />
        <Radar
          name='AthletsPerCountry'
          dataKey='amount'
          stroke='#8884d8'
          fill='#8884d8'
          fillOpacity={0.6}
        />
      </RadarChart>
    </ResponsiveContainer>
  );
};

export default AthletePerCountry;
