import React from 'react';
import styled from 'styled-components';
import Carousel from 'react-material-ui-carousel'
import { Paper, Button } from '@material-ui/core'
import { useHistory } from "react-router";
import Map from "../reuseable/Map";

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 500px;
`

const ImageDesc = styled.div`
  display: flex;
`

const MainWrapper = styled.div`
  width: 40vw;
`

const MuseumCarousel = (props) => {
  const {data} = props;

  return (
    <MainWrapper>
      <h1>Related Museums</h1>
      <Carousel navButtonsAlwaysVisible={true}>
      {
        data.map( (item, i) => <Item key={i} item={item} /> )
      }
      </Carousel>
    </MainWrapper>
  )
}

const Item = (props) =>
{
  const {item} = props;
  const history = useHistory();

  return (
    <Paper>
      <ContentWrapper>
        <h2>{item.name}</h2>
        <ImageDesc>
          <img style={{alignSelf: 'center', margin: '0 1rem'}} src={item.image} height="300px" alt={item.name}/>
          <Map center={{lat: item.lat, lng: item.lng}} height="300px"/>
          {/* <p>{item.website}</p> */}
        </ImageDesc>
        <Button onClick={()=>{history.push('/museums/' + item.id)}}>
          View
        </Button>
      </ContentWrapper>
    </Paper>
  )
}

export default MuseumCarousel
