import React, {useEffect} from 'react';
import styled from 'styled-components';
import MovementTable from "./MovementTable";

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

function MovementFinderRoute() {

  useEffect(() => {
    document.title = "Gallery Gaze - Movement"
  }, [])

  return (
    <MainWrapper>
      <h1>Movements</h1>
      <MovementTable/>
    </MainWrapper>
  );
}

export default MovementFinderRoute;
