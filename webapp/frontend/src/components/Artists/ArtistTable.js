import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { DataGrid } from '@mui/x-data-grid';
import { makeStyles } from '@mui/styles';
import SearchBar from "../reuseable/SearchBar";
import { Button } from "@mui/material";
import FilterSelect from "../reuseable/FilterSelect";

const useStyles = makeStyles({
  root: {
    '& .MuiDataGrid-columnHeaderTitle': {
      fontWeight: 'bold',
    },
  },
});

const TableWrapper = styled.div`
  width: '100%';
  font-weight: 'bold';
  margin: 2rem 0;
`;

const SearchWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`

const ArtistTable = (props) => {
  const classes = useStyles();
  const history = useHistory();

  const [data, setData] = useState({entries:[], total: 0});
  const [pageNum, setPageNum] = useState(0);
  const [searchVal, setSearchVal] = useState("");
  const [instancesPerPage, setInstancesPerPage] = useState(25);
  const [selectedFilterState, setSelectedFilterState] = useState({});
  const [filterParamString, setFilterParamString] = useState("");
  const [sortParamString, setSortParamString] = useState("");

  useEffect(() => {
    updateTableData(pageNum, instancesPerPage);
  }, [pageNum, instancesPerPage, searchVal, selectedFilterState, filterParamString, sortParamString]);

  const artistFilterTitles = [
    "Name", "Birth Year", "Nationality", "Gender", "Popularity Ranking", "Number of Artworks"
  ];

  const artistFilters = [
    [
      "A-E", "F-L", "M-P", "Q-Z"
    ],
    [
      "1200s", "1300s", "1400s", "1500s", "1600s", "1700s", "1800s", "1900s"
    ],
    [
      "French", "Italian", "Russian", "British", "German", "American", "Dutch", "Spanish", "Ukrainian", "Other"
    ],
    [
      "Male", "Female"
    ],
    [
      "1-10", "11-20", "21-30", "31-40", "41-50", "51-100", "101+"
    ],
    [
      "1", "2", "3", "4", "5", "6+"
    ]
  ];

  const filterMapping = {
    "number_of_artworks": "artwork_count",
    "birth_year": "birthday"
  }

  const mapFilterValues = (filter) => {
    if (filter in filterMapping) {
      return filterMapping[filter];
    }
    return filter;
  }

  const onCheckboxClick = (title, value) => {
    let newState = selectedFilterState;

    if (title in newState) {
      if (newState[title].includes(value)) {
        newState[title] = newState[title].filter(item => item !== value);
        if (newState[title].length === 0) {
          delete newState[title];
        }
      } else {
        newState[title] = [...newState[title], value];
      }
    } else {
      newState[title] = [value];
    }
    let filterParams = `&filter=`;
    for (let key in newState) {
      const newKey = String(key).replaceAll(" ", "_");
      filterParams += `${mapFilterValues(String(newKey).toLowerCase())}=${newState[key].map(e => mapFilterValues(e)).join(",").toLowerCase()};`;
    }
    // get rid of last ;
    filterParams = filterParams.slice(0, -1);

    if (filterParams !== "&filter=") {
      setFilterParamString(filterParams.replace("+", "%2B"));
    } else {
      setFilterParamString("");
    }

    setSelectedFilterState(newState);
  }

  const columns = [
    { field: 'name', headerName: 'Name', renderCell: (params)=> (
      highlightText(String(params.value))
    ), flex: 1},
    { field: 'birthday', headerName: 'Date of Birth', renderCell: (params)=> (
      highlightText(String(params.value))
    ), flex: 1 },
    { field: 'gender', headerName: 'Gender', renderCell: (params)=> (
      highlightText(String(params.value))
    ), flex: 1 },
    { field: 'nationality', headerName: 'Nationality', renderCell: (params)=> (
      highlightText(String(params.value))
    ), flex: 1 },
    { field: 'artwork_count', headerName: 'Number of Artworks', renderCell: (params)=> (
      highlightText(String(params.value))
    ), flex: 1 },
    { field: 'popularity_ranking', headerName: 'Popularity Ranking', renderCell: (params)=> (
      highlightText(String(params.value))
    ), flex: 1 },
  ];
  
  const handleSearch = (searchValue) => {
    setSearchVal(searchValue);
    setPageNum(0);
  };

  const sortByColumn = (e) => {
    if (e.length <= 0) return;

    let sortParam = `&sort=${e[0].field}&sortReverse=${e[0].sort === 'asc' ? 'false' : 'true'}`;
    setSortParamString(sortParam);
  }

  const updateTableData = (page, pageSize) => {
    console.log("Updating table")
    const newPageNum = page + 1;
    let query =`https://api.beta.gallerygaze.me/search?model=artist${searchVal !== '' ? `&general_string=${searchVal}` : ''}&page=${newPageNum}&maxResults=${pageSize}`;

    query += filterParamString;
    query += sortParamString;
    console.log(String(query));

    // remove &filter if there are none selected
    const endVal = query.length;
    if (query.slice(endVal - 7, endVal) === '&filter') {
      query = query.slice(0, endVal - 7);
    }

    console.log(query);
    fetch(query)
    .then((response) => {
      if (response.ok) return response.json();
      throw response;
    })
    .then((res) => {
      console.log(res);
      res.entries = res["query results"];
      setData(res);
    })
    .catch((err) => console.error('ERROR FETCH: ', err));
  };

  // highlight text that matches search
  const highlightText = (text) => {
    try {
      // Split text on highlight term, include term itself into parts, ignore case
      const parts = text.split(new RegExp(`(${searchVal})`, 'gi'));
      return <span>{parts.map(part => part.toLowerCase() === searchVal.toLowerCase() ? <mark>{part}</mark> : part)}</span>;
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <TableWrapper className={classes.root}>
      <FilterSelect
        filterTitles = {artistFilterTitles}
        modelFilters = {artistFilters}
        onClick={onCheckboxClick}
      />
      <SearchWrapper>
        <SearchBar onSearch={handleSearch}/>
      </SearchWrapper>
      <DataGrid
        pagination
        paginationMode="server"
        rowCount={data.total}
        onPageChange={setPageNum}
        autoHeight={true}
        rows={data.entries}
        columns={columns}
        pageSize={instancesPerPage}
        onPageSizeChange={setInstancesPerPage}
        disableSelectionOnClick={true}
        onRowClick={(row) => history.push({pathname: '/artists/' + row.id, state:{searchTerm: searchVal}})}
        onSortModelChange={sortByColumn}
      />
    </TableWrapper>
  );
};

export default ArtistTable;
