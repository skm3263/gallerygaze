.DEFAULT_GOAL := all
SHELL         := bash

# docker:
# fill in docker command here

all:

C_FILES :=			\
	.gitignore		\
	.gitlab-ci.yml

check: $(C_FILES)

setup-frontend:
	cd webapp/frontend/ && npm install

build-frontend:
	cd webapp/frontend/ && npm build

run-frontend-dev:
	cd webapp/frontend/ && npm start

build-backend-docker:
	docker build -t backend ./backend

run-backend-docker:
	docker run --rm -p 8000:8000 backend 