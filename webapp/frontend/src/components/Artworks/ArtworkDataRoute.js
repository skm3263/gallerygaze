import React, {useState, useEffect} from 'react'
import { Link, useHistory } from 'react-router-dom';
import styled from 'styled-components';
import artworkData from '../../data/artwork.json';
import { useParams } from 'react-router-dom';
import Paper from '@mui/material/Paper';
import ArtistPreview from "../Artists/ArtistPreview";
import MuseumPreview from "../Museums/MuseumPreview";
import Map from "../reuseable/Map"

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 4rem;
`;

const MiddleSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`

const LeftSide = styled.div`
  width: 30%;
  `
const RightSide = styled.div`
  width: 50%; 
`

const Previews = styled.div`
  display:flex;
`

const RedirectLink = styled(Link)`
  text-decoration: none;
  color: black;
`;
 
const getData = (id) => artworkData.find(e => e.id === id)

const ArtworkDataRoute = (props) => {
  const { id } = useParams();
  const [data, setData] = useState({});
  const history = useHistory();

  useEffect(() => {
    fetch("https://api.beta.gallerygaze.me/artworks/" + id)
    .then(response => {
      if (response.ok)
        return response.json();
      throw response
    })
    .then(data => {
      console.log(data)
      setData(data)
    })
    .catch(err => console.error("ERROR FETCH: ", err))
  }, [])

  const searchVal = props.location.state?.searchTerm ?? '';

  // highlight text that matches search
  const highlightText = (text) => {
    try {
      const textStr = String(text);
      // Split text on highlight term, include term itself into parts, ignore case
      const parts = textStr.split(new RegExp(`(${searchVal})`, 'gi'));
      return <span>{parts.map(part => part.toLowerCase() === searchVal.toLowerCase() ? <mark>{part}</mark> : part)}</span>;
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <MainWrapper>
      {console.log(searchVal)}
      <h1>{highlightText(data.name) ?? 'loading'}</h1>
      {data && 
      <MiddleSection>
        <LeftSide>
          <img src={data.image} width="500px" alt={data.name}/>
        </LeftSide>
        <RightSide>
          <Paper sx={{padding: '2rem', margin: '1rem'}}>
            <p>{highlightText(data.desc)}</p> 
            <p> <b>Medium:</b> {highlightText(data.media)} </p>
            <p> <b>Movement:</b> {highlightText(data.movement_name)}</p>
            <p> <b>Genre:</b> {highlightText(data.genre)}</p>
            <p> <b>Height:</b> {highlightText(data.height)}</p>
            <p> <b>Width:</b> {highlightText(data.width)}</p>
            <p> <b>Popularity Ranking:</b> {highlightText(data.popularity_ranking)}</p>
            <p> <b>Creation Date:</b> {highlightText(data.date_created)}</p>
          </Paper>
          <Previews>
            <Paper sx={{padding: '2rem', margin: '1rem', width: '100%'}}>
              <ArtistPreview data={data?.artist} onClick={()=>history.push(`/artists/${data?.artist?.id}`)} />  
            </Paper>
            <Paper sx={{padding: '2rem', margin: '1rem', width: '100%'}}>
              <MuseumPreview data={data?.museum} onClick={()=>history.push(`/museums/${data?.museum?.id}`)} />  
            </Paper>
            {data && data.museum && <Map center={{lat: data?.museum.lat, lng: data?.museum.lng}}/> }
          </Previews>
        </RightSide>
      </MiddleSection>
      }
    </MainWrapper>
  );
}

export default ArtworkDataRoute;
