import React, { Component } from 'react';
import styled from 'styled-components';
import StaticImage from "./reuseable/StaticImage";

import TeamMembers from './TeamMembers'

const axios = require('axios');

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  align-items: center;
`;

const PushMargin = styled.div`
  margin-bottom: 45px;
`;

const BioComponent = styled.div`
  width: 300px;
  margin: auto;
  text-align: center;
`;

const InfoComponent = styled.div`
  width: 400px;
  text-align: center;
`;

const NameComponent = styled.div`
  font-size: 30px;
  font-weight: bold;
`;

const SectionTitle = styled.div`
  font-size: 30px;
  font-style: italic;
  display: flex;
  justify-content: center;
  margin: 1rem 1rem;
`;

const ItalicWrapper = styled.div`
  font-style: italic;
`;

const Names = styled.div`
  width: 75%;
  margin: auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const DataSection = styled.div`
  display: flex;
`

class AboutRoute extends Component {

  /* CITATION: 
     The code to get the stats (commits and issues) from the GitLab API is inspired by
     and derived from Spring '21 11am group readyrecipes. The link to their repo:
     https://gitlab.com/cs373-group13/readyrecipes/-/tree/master 
     The design of the About page is our own */

  state = {
    repoCommitStats: [],
    repoTotalCommits: Number,
    repoTotalIssues: Number,
    repoClosedIssues: Number,
    teamMembers: TeamMembers
  }
  componentDidMount() {
    axios.get(`https://gitlab.com/api/v4/projects/29921560/repository/contributors`)
      .then(response => {
        const commits = response.data;
        this.setState({ ...this.state, repoCommitStats: commits});
        this.creditCommitStats();
      });
    this.creditIssueStats();
  }

  creditCommitStats() {
    console.log(this.state.repoCommitStats);
    let membersCopy = this.state.teamMembers;
    let commitCount = 0;
    let shansExtra = 0;
    for (let i = 0; i < this.state.repoCommitStats.length; i++) {
      commitCount += this.state.repoCommitStats[i].commits;
      if (this.state.repoCommitStats[i].email === "shankmemon@utexas.edu") {
        shansExtra = this.state.repoCommitStats[i].commits;
        continue;
      }
      for (let j = 0; j < this.state.teamMembers.length; j++) {
        if (this.state.repoCommitStats[i].email === this.state.teamMembers[j].email) {
          membersCopy[j].commits = this.state.repoCommitStats[i].commits;
        }
      }
    }
    // Shan accidentally committed under multiple emails.
    membersCopy[3].commits += shansExtra;
    this.setState({teamMembers: membersCopy, repoTotalCommits: commitCount})
  }

  creditIssueStats() {
    axios.get(`https://gitlab.com/api/v4/projects/29921560/issues_statistics`)
    .then(response => {
      const totalIssues = response.data.statistics.counts.all;
      const closedIssues = response.data.statistics.counts.closed;
      this.setState({repoTotalIssues: totalIssues, repoClosedIssues: closedIssues});
    })
    for (let i = 0; i < this.state.teamMembers.length; i++) {
      axios.get(`https://gitlab.com/api/v4/projects/29921560/issues_statistics?author_username=` + this.state.teamMembers[i].gitlabid)
      .then(response => {
        const curIssues = response.data.statistics.counts.all;
        // We have access to the response only right here. We will have to make
        // a copy of the team info each time and update the state each iteration
        let membersCopy = [...this.state.teamMembers];
        let thisMembersIssues = {...membersCopy[i]};
        thisMembersIssues.issues = curIssues;
        membersCopy[i] = thisMembersIssues;
        this.setState({teamMembers: membersCopy});
      })
    }
  }

  // End citation. Proceding code is 100% our own.

  render() {
    function Person(props) {
      return (
        <PushMargin>
          <div style={{margin: "0 0 0 1rem"}}>
            <NameComponent>
              {props.name}
            </NameComponent>
            <ItalicWrapper>
              {props.job}
            </ItalicWrapper>
            <div>Commits: {props.commits}</div>
            <div>Issues: {props.issues}</div>
          </div>
          <div>
            <StaticImage image={props.profilePic} height={200} width={null} resizeMode={"contain"}/>
          </div>
          <BioComponent>
            {props.bio}
          </BioComponent>
        </PushMargin>
      );
    }

    return (
      <MainWrapper>
        <PushMargin>
          <PushMargin>
            <SectionTitle>
              What is Gallery Gaze?
            </SectionTitle>
          </PushMargin>
          <InfoComponent>
            We're a team of developers working together to make a user friendly app for both the art aficionado and amateur! Gallery Gaze seeks to make an interactive database of art of all mediums, artists, and even museums across the world. Gaze for hours at your favorite pieces, discover your new favorite artists, or find a museum with amazing exhibits.
          </InfoComponent>
        </PushMargin>
        <PushMargin>
          <SectionTitle>
            Meet the Team!
          </SectionTitle>

          <Names>
            {this.state.teamMembers.map((member) => (
              <Person 
                name = {member.name}
                profilePic = {member.photo}
                bio = {member.bio}
                commits = {member.commits}
                issues = {member.issues}
                job = {member.job}
                key = {member.gitlabid}/>
            ))}
          </Names>

        </PushMargin>
        <DataSection>
          <PushMargin>
            <PushMargin>
              <SectionTitle>
                API Links & info
              </SectionTitle>
            </PushMargin>

            <div style={{margin: '1rem 1rem'}}>
              <div>
                Links to used APIs (All were scraped using scripts in backend/scripts):
              </div>
              <ItalicWrapper>
                <a href="https://developers.artsy.net/" rel="noreferrer">
                  developers.artsy.net
                </a>
              </ItalicWrapper>
              <ItalicWrapper>
                <a href="https://www.wikiart.org/en/App/GetApi" rel="noreferrer">
                  wikiart.org/en/App/GetApi
                </a>
              </ItalicWrapper>
              <ItalicWrapper>
                <a href="https://developers.google.com/maps/documentation/places/web-service/overview" rel="noreferrer">
                developers.google.com/maps/documentation/places/web-service/overview
                </a>
              </ItalicWrapper>
            </div>
          </PushMargin>
          
          <PushMargin>
            <PushMargin>
              <SectionTitle>
                Tools
              </SectionTitle>
            </PushMargin>
            <InfoComponent>
              On a large scale, we are using VS Code to write code. Git and GitLab help us with 
              version control, issue tracking, iterative development, and overall collaboration.
              To get this site up and running, we created a JavaScript (node.js) frontend using the
              React library to use reactive states and make it easier to render HTML components.
              Within JS and React, we used Material UI and styled components to give design to
              the site and make it look cleaner and more user friendly.
              For the backed, we used Flask as our framework and PostgreSQL for our database. 
              For deployment, we used AWS Amplify. For our visualizations, we used recharts.
              We used Docker to virtualize a container for our project. We also used Postman to design an API.
            </InfoComponent>
            
          </PushMargin>
        </DataSection>
        <DataSection>
          <PushMargin>
            <PushMargin>
              <SectionTitle>
                Links to Our Work
              </SectionTitle>
            </PushMargin>
            <InfoComponent>
              <ItalicWrapper>
                <a href = "https://gitlab.com/skm3263/gallerygaze/-/tree/main" rel = "noreferrer">
                  Our gitlab repo
                </a>
              </ItalicWrapper>
              <ItalicWrapper>
                <a href = "https://documenter.getpostman.com/view/17693376/UV5dctiB" rel = "noreferrer">
                  Our API on Postman
                </a>
              </ItalicWrapper>
            </InfoComponent>
          </PushMargin>
          <PushMargin>
            <PushMargin>
              <SectionTitle>
                Team Stats
              </SectionTitle>
            </PushMargin>
            <InfoComponent>
              <div>Total team commits: {this.state.repoTotalCommits}</div>
              <div>Total issues: {this.state.repoTotalIssues}</div>
              <div>Closed issues: {this.state.repoClosedIssues}</div>
              <div>jest (JavaScript) tests: 24</div>
              <div>Selenium GUI tests: 13</div>
              <div>Backend unit tests: 12</div>
              <div>Postman API tests: 56</div>
            </InfoComponent>
          </PushMargin>
        </DataSection>
      </MainWrapper>
    );
  }
}

export default AboutRoute;
