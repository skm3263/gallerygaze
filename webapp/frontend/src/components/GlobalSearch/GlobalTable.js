import React, { useState, useEffect, useRef } from 'react';
import styled from 'styled-components';
import { Button } from '@mui/material';
import SearchBar from "../reuseable/SearchBar";
import GlobalCard from "./GlobalCard";

const MainWrapper = styled.div``;

const CardsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const BottomRow = styled.div`
  display: flex;
  justify-content: end;
  margin: 1rem 8rem;
  p {
    margin-right: 1rem;
  }
`;

const SearchWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`

const GlobalTable = (props) => {  

  const {searchValSent} = props;

  const [data, setData] = useState({entries:[], total: 0});
  const [searchVal, setSearchVal] = useState("");
  const [instancesPerPage, setInstancesPerPage] = useState(25);
  const [pageNum, setPageNum] = useState(0);
  const [totalInstances, setTotalIntances] = useState(0);

  const searchBar = useRef(null);

  useEffect(() => {
    console.log("search val sent" + searchValSent)
    if(searchValSent){
      setSearchVal(searchValSent);
    }
  }, [searchValSent]);

  useEffect(() => {
    updateTableData(pageNum, instancesPerPage);
  }, [pageNum, instancesPerPage, searchVal]);

  // const columns = [
  //   { field: 'name', headerName: 'Artwork', flex: 1},
  //   { field: 'museum_name', headerName: 'Museum', flex: 1 },
  //   { field: 'styles', headerName: 'Movement', flex: 1 },
  //   { field: 'popularity_ranking', headerName: 'Popularity Ranking', flex: 1 },
  //   { field: 'date_created', headerName: 'Creation Date', flex: 1 },
  // ];
  const handleSearch = (searchValue) => {
    console.log("searching for " + searchValue);
    setSearchVal(searchValue);
    setPageNum(0);
  };

  const updateTableData = (page, pageSize) => {
    if (searchVal === "") return;

    const newPageNum = page + 1;
    console.log(newPageNum);
    const query = `https://api.beta.gallerygaze.me/search?general_string=${searchVal}&page=${newPageNum}&maxResults=${pageSize}`;
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((res) => {
        console.log(res);
        setTotalIntances(res.total);
        const artists = [...res.artists].map(artist => {artist.type="artists"; artist.key=artist.id; return artist});
        const artworks = [...res.artworks].map(artwork => {artwork.type="artworks"; artwork.key=artwork.id; return artwork});
        const museums = [...res.museums].map(museum => {museum.type="museums"; museum.key=museum.id; return museum});
        const movements = [...res.movements].map(movement => {movement.type="movements"; movement.key=movement.id; return movement});
        res.entries = [...artworks, ...museums, ...movements, ...artists];
        setData(res);
      })
      .catch((err) => console.error('ERROR FETCH: ', err));
  };

  return (
    <MainWrapper>
      <SearchWrapper>
        <SearchBar defaultValue={searchValSent} onSearch={handleSearch}/>
      </SearchWrapper>
      <CardsWrapper>
        {data.entries.map((entry) => (
          <GlobalCard key={entry.id} data={entry} searchTerm={searchVal}/>
        ))}
      </CardsWrapper>
      {/* <BottomRow>
        <p>Rows per page: {instancesPerPage}</p>
        <p>
          {pageNum * instancesPerPage}-
          {Math.min(totalInstances, pageNum * instancesPerPage + instancesPerPage)}{' '}
          of {totalInstances}{' '}
        </p>
        <Button disabled={pageNum === 0} onClick={() => setPageNum(pageNum - 1)}>
          Prev
        </Button>
        <Button
          disabled={pageNum === Math.floor(totalInstances / instancesPerPage)}
          onClick={() => setPageNum(pageNum + 1)}
        >
          Next
        </Button>
      </BottomRow> */}
    </MainWrapper>
  );
};

export default GlobalTable;
