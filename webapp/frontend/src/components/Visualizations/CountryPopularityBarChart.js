import React, { useEffect, useState } from 'react';
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';

const CountryPopularityBarChart = () => {
  useEffect(() => {
    let data = [];
    let countryData = {};

    const query =
      'https://api.beta.gallerygaze.me/search?model=museum&page=1&maxResults=500';
    fetch(query)
      .then((response) => {
        if (response.ok) return response.json();
        throw response;
      })
      .then((res) => {
        data = res['query results'];

        // countryData is a dictionary with Country as key and sum of all popularity_ranking as value
        countryData = data.reduce((acc, curr) => {
          if (acc[curr.country]) {
            acc[curr.country] += curr.popularity_ranking;
          } else {
            acc[curr.country] = curr.popularity_ranking;
          }
          return acc;
        }, {});

        let newData = [];
        for (let key in countryData) {
          if (key === "Wherever") continue;
          newData.push({
            name: key,
            ranking: countryData[key]
          })
        }
        // sort newData by ranking
        newData.sort((a, b) => {
          return a.ranking - b.ranking;
        });
        // keep only top 25
        newData = newData.slice(0, 20);
        setData(newData);
      })
      .catch((err) => console.error('ERROR FETCH: ', err));
  }, []);

  const [data, setData] = useState([]);
  const tickFormatter = (value, index) => {
    const limit = 8; // put your maximum character
    if (value.length < limit) return value;
    return `${value.toString().substring(0, limit)}...`;
  };

  return (
    <ResponsiveContainer width="100%" height={300}>
    <BarChart
      width={2000}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray='3 3' />
      <XAxis dataKey='name' tickFormatter={tickFormatter}/>
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey='ranking' fill='#82ca9d' />
    </BarChart>
    </ResponsiveContainer>
  );
};

export default CountryPopularityBarChart;
