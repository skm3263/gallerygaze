import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { useHistory } from "react-router";

const GlobalCard = (props) => {
  const {data, searchTerm} = props
  const type = data.type
  const history = useHistory();

  // highlight text that matches search
  const highlightText = (text) => {
    const textStr = String(text)
    // Split text on highlight term, include term itself into parts, ignore case
    const parts = textStr.split(new RegExp(`(${searchTerm})`, 'gi'));
    return <span>{parts.map(part => part.toLowerCase() === searchTerm.toLowerCase() ? <mark>{part}</mark> : part)}</span>;
  };

  const handleClick = () => {
    history.push({pathname: `/${type}/${data.id}`, state:{searchTerm: searchTerm}})
  }

  return (
    <Card sx={{ width: 345, margin: "1rem" }}>
      {data && 
      <CardActionArea onClick={handleClick}>
        <CardMedia
          component="img"
          height="140"
          image={data.image}
          alt={data.name}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {highlightText(data.name)}
          </Typography>

          {type === "artworks" && (
          <>
            <Typography variant="body2" color="text.secondary">
              Genre: {highlightText(data.genre)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Movement: {highlightText(data.movement_name)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Medium: {highlightText(data.media)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Creation Date: {highlightText(data.date_created)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Popularity Ranking: {highlightText(data.popularity_ranking)}
            </Typography>
          </>)}

          {type === "museums" && (
          <>
            <Typography variant="body2" color="text.secondary">
              Museum: {highlightText(data.name)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Address: {highlightText(data.address)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Website: {highlightText(data.website)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Phone: {highlightText(data.phone)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Status: {highlightText(data.status)}
            </Typography>
          </>)}

          {type === "artists" && (
          <>
            <Typography variant="body2" color="text.secondary">
              Artist: {highlightText(data.name)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Birthday: {highlightText(data.birthday)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Date of Death: {highlightText(data.deathday)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Hometown: {highlightText(data.hometown)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Nationality: {highlightText(data.nationality)}
            </Typography>
          </>)}

          {type === "movements" && (
          <>
            <Typography variant="body2" color="text.secondary">
              Movement: {highlightText(data.name)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Number of Artworks: {highlightText(data.artwork_count)}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Desc: {highlightText(data.desc.substring(0, 40))}...
            </Typography>
          </>)}

        </CardContent>
      </CardActionArea>}
    </Card>
  )
}

export default GlobalCard
