import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { useHistory } from "react-router";

const ArtworkCard = (props) => {
  const {data, searchTerm} = props
  const history = useHistory();
  
  // highlight text that matches search
  const highlightText = (text) => {
    try {
      const textStr = String(text)
      // Split text on highlight term, include term itself into parts, ignore case
      const parts = textStr.split(new RegExp(`(${searchTerm})`, 'gi'));
      return <span>{parts.map((part, i) => part.toLowerCase() === searchTerm.toLowerCase() ? <mark key={i}>{part}</mark> : part)}</span>;
    } catch (error) {
      console.error(error)
    }
  };

  return (
    <Card sx={{ width: 345, margin: "1rem" }}>
      {data && 
      <CardActionArea onClick={() => history.push({pathname: '/artworks/' + data.id, state:{searchTerm: searchTerm}})}>
        <CardMedia
          component="img"
          height="140"
          image={data.image}
          alt={data.name}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {highlightText(data.name)}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Genre: {highlightText(data.genre)}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Movement: {highlightText(data.movement_name)}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Medium: {highlightText(data.media)}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Creation Date: {highlightText(data.date_created)}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Popularity Ranking: {highlightText(data.popularity_ranking)}
          </Typography>
        </CardContent>
      </CardActionArea>}
    </Card>
  )
}

export default ArtworkCard
